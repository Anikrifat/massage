<?php include('inc/header.php') ?>
<div class="content mt-5 pt-5">
  <div class="con-wrap con-subpage ">
    <button class="detail-condition-toggle" onclick="detailMenuToggle(this)">
      <font style="vertical-align: inherit;">
        <font style="vertical-align: inherit;">conditional search</font>
      </font>
    </button>

    <form method="post" id="filter_src" name="filter_src" enctype="multipart/form-data" onsubmit="return false;">
      <div class="detail-condition" id="searchfilter">
        <nav class="detail-condition-nav">
          <p class="tit">
            <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">filter</font>
            </font>
          </p>
          <ul class="detail-condition-list">
            <li class="type">
              <div class="area">
                <ul class="check-list half">
                  <li>
                    <div class="checks">
                      <!--checked-->
                      <input type="checkbox" id="typeOpen" name="ingType" value="1">
                      <label for="typeOpen">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">open shop</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="typeNow" name="payType" value="1">
                      <label for="typeNow">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">direct payment</font>
                        </font>
                      </label>
                    </div>
                  </li>
                </ul>
              </div>
            </li>
            <li class="price">
              <div class="tit">
                <p class="tit">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">price</font>
                  </font>
                </p>
              </div>
              <div class="area">
                <p class="range-price">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">0$ to 1000$</font>
                  </font>
                </p>
                <span class="irs irs--sharp js-irs-4">
                <input type="range" class="range" multiple value="40,60" data-drag-middle />
              </div>
            </li>
            <!-- <li class="course">
              <p class="tit">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">course available</font>
                </font>
              </p>
              <div class="area">
                <ul class="course-list">
                  <li class="all courseall active" data-value="0">
                    <a href="javascript:;">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">all</font>
                      </font>
                    </a>
                  </li>
                  <li class="course7" data-value="7">
                    <a href="javascript:;">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">massage</font>
                      </font>
                    </a>
                  </li>
                  <li class="course8" data-value="8">
                    <a href="javascript:;">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">aesthetics</font>
                      </font>
                    </a>
                  </li>
                  <li class="course16" data-value="16">
                    <a href="javascript:;">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">waxing</font>
                      </font>
                    </a>
                  </li>
                </ul>
                <div class="course-detail">
                  <ul class="course-detail-list detail second cd-7">
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Thailand</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c1" data-col="1">
                        <li class="all active" data-value="127">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">tie management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Aroma management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="4">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">foot care</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="8">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Swedish Management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="16">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">couple management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="32">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">spa management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="64">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">other management</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">China</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c2" data-col="2">
                        <li class="all active" data-value="31">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">sports management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Aroma management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="4">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">foot care</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="8">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">couple management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="16">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">other management</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Korea</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c4" data-col="3">
                        <li class="all active" data-value="1023">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">dry management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Aroma management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="4">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">foot care</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="8">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Swedish Management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="16">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">couple management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="32">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">acupressure management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="64">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">meridian management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="128">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">correction management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="256">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">spa management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="512">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">other management</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="course-detail-list detail second cd-8">
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">face management</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c8" data-col="4_1">
                        <li class="all active" data-value="1023">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Trouble management (acne / marks / scars)</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Whitening management (melasma, freckles, blemishes)</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="4">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Elasticity management (wrinkle, aging)</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="8">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">pore management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="16">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Play management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="32">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">moisture management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="64">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">keratin care</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="128">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Detox management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="256">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">lifting management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="512">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">small face care</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">body care</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c8" data-col="4_2">
                        <li class="all active" data-value="63">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">body care</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">body shape management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="4">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Detox management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="8">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">flexible management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="16">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">slimming management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="32">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">scrub management</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">other management</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c8" data-col="4_3">
                        <li class="all active" data-value="63">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">wedding management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">prenatal care</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="4">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">postpartum care</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="8">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">couple management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="16">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Management for women only</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="32">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">other management</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                  <ul class="course-detail-list detail second cd-16">
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">type of wax</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c16" data-col="5_0">
                        <li class="all active" data-value="3">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Soft/Hard Wax</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Sugaring Wax</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">waxing care</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c16" data-col="5_1">
                        <li class="all active" data-value="7">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Brazillian waxing</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">face waxing</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="4">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">body waxing</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <p class="tit">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">other management</font>
                        </font>
                      </p>
                      <ul class="course-detail-list c16" data-col="5_2">
                        <li class="all active" data-value="15">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">all</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="1">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">treatment management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="2">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">couple management</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="4">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">Management for women only</font>
                            </font>
                          </a>
                        </li>
                        <li data-value="8">
                          <a href="javascript:;">
                            <font style="vertical-align: inherit;">
                              <font style="vertical-align: inherit;">other management</font>
                            </font>
                          </a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </div>
              </div>
            </li> -->
            <!-- <li>
              <p class="tit">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Now this time instant discount ★</font>
                </font>
              </p>
              <div class="area">
                <ul class="now-sale-list">
                  <li class="all active">
                    <a href="javascript:;">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">all</font>
                      </font>
                    </a>
                  </li>

                  <li class="onsale1">
                    <a href="javascript:;">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Weekly discount</font>
                      </font>
                    </a>
                  </li>

                  <li class="onsale2">
                    <a href="javascript:;">
                      <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">couple discount</font>
                      </font>
                    </a>
                  </li>

                </ul>
              </div>
            </li> -->
            <!-- <li class="event">
              <p class="tit">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">free coupon event</font>
                </font>
              </p>
              <div class="area">
                <ul class="check-list">
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="eventAll" class="all" name="event" value="all" checked="" onclick="listChk(this)">
                      <label for="eventAll">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">all</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="eventOngoing" name="event" value="1" onclick="listChk(this)">
                      <label for="eventOngoing">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Proceeding</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="eventEnd" name="event" value="0" onclick="listChk(this)">
                      <label for="eventEnd">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">not in progress</font>
                        </font>
                      </label>
                    </div>
                  </li>
                </ul>
              </div>
            </li> -->
            <li class="service">
              <p class="tit"><a href="javascript:void(0)" onclick="detailToggle(this)">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">service available</font>
                  </font>
                </a></p>
              <div class="area">
                <ul class="check-list detail-event-list close">
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceAll" class="all" name="possibleServiceAll" value="458511" checked="" onclick="listChk(this)">
                      <!--checked-->
                      <label for="possibleServiceAll">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">all</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceOneDay" name="possibleService[]" value="1" onclick="listChk(this)">
                      <label for="possibleServiceOneDay">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">open 24 hours</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceFreeParking" name="possibleService[]" value="2" onclick="listChk(this)">
                      <label for="possibleServiceFreeParking">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">free parking</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceChargeParking" name="possibleService[]" value="4" onclick="listChk(this)">
                      <label for="possibleServiceChargeParking">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">paid parking</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceSleepFree" name="possibleService[]" value="8" onclick="listChk(this)">
                      <label for="possibleServiceSleepFree">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">free sleep</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceSleep" name="possibleService[]" value="131072" onclick="listChk(this)">
                      <label for="possibleServiceSleep">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">paid sleep</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceSleepCondition" name="possibleService[]" value="262144" onclick="listChk(this)">
                      <label for="possibleServiceSleepCondition">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">conditional sleep</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceRoomCouple" name="possibleService[]" value="256" onclick="listChk(this)">
                      <label for="possibleServiceRoomCouple">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">couple room</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceRoomGroup" name="possibleService[]" value="512" onclick="listChk(this)">
                      <label for="possibleServiceRoomGroup">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">group room</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceWifi" name="possibleService[]" value="1024" onclick="listChk(this)">
                      <label for="possibleServiceWifi">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Wi-Fi</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceRoomPersonal" name="possibleService[]" value="2048" onclick="listChk(this)">
                      <label for="possibleServiceRoomPersonal">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">private room</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceShower" name="possibleService[]" value="4096" onclick="listChk(this)">
                      <label for="possibleServiceShower">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">shower available</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceCalendar" name="possibleService[]" value="8192" onclick="listChk(this)">
                      <label for="possibleServiceCalendar">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Reservation required</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceToilet" name="possibleService[]" value="16384" onclick="listChk(this)">
                      <label for="possibleServiceToilet">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Separate male and female toilets</font>
                        </font>
                      </label>
                    </div>
                  </li>
                  <li>
                    <div class="checks">
                      <input type="checkbox" id="possibleServiceDog" name="possibleService[]" value="32768" onclick="listChk(this)">
                      <label for="possibleServiceDog">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Pets allowed</font>
                        </font>
                      </label>
                    </div>
                  </li>
                </ul>
              </div>
            </li>
            <li class="score">
              <div class="tit">
                <p class="tit">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">grade</font>
                  </font>
                </p>
              </div>
              <div class="area">
                <p class="range-price">
                  <font style="vertical-align: inherit;">
                    <font style="vertical-align: inherit;">1 to 5 points</font>
                  </font>
                </p>
                <span class="irs irs--sharp js-irs-4">
                <input type="range" class="range" multiple value="40,60" data-drag-middle />
              </div>
            </li>
          </ul>
          <div class="detail-condition-btn fixed">
            <button type="button" class="reset" onclick="detailReset(this)">
              <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">initialization</font>
              </font>
            </button>
            <button type="button" class="apply" onclick="filtersrc()">
              <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">to apply</font>
              </font>
            </button>
          </div>
        </nav>
        <button type="button" class="detail-close" onclick="detailMenuToggle(this)">
          <span class="blind">
            <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">Advanced search toggle button</font>
            </font>
          </span>
        </button>
      </div>
    </form>
    <!-- 본문 -->
    <section class="sub-page">

      <h2 class="sub-page-tit">
        <span class="subject">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">Recommended shops around me</font>
          </font>
        </span>
      </h2>

      <article class="shop-area">
        <div class="tab-area">
          <ul class="tab-list shop-tab">
            <li class="courseall active">
              <a href="javascript:;" onclick="massageClick(this, 0)">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">all</font>
                </font>
              </a>
            </li>
            <li class="course7">
              <a href="javascript:;" onclick="massageClick(this, 7)">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">massage</font>
                </font>
              </a>
            </li>
            <!-- <li class="course8">
              <a href="javascript:;" onclick="massageClick(this, 8)">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">aesthetics</font>
                </font>
              </a>
            </li>
            <li class="course16">
              <a href="javascript:;" onclick="massageClick(this, 16)">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">waxing</font>
                </font>
              </a>
            </li> -->
          </ul>
          <ul class="tab-list shop-tab-sub hide">
            <li class="course7 active" data-value="7">
              <a href="javascript:;" class="all" onclick="massageClickSub(this, event)">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">all</font>
                </font>
              </a>
            </li>
            <li class="course1" data-value="1">
              <a href="javascript:;" onclick="massageClickSub(this, event)">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Thailand</font>
                </font>
              </a>
            </li>
            <li class="course2" data-value="2">
              <a href="javascript:;" onclick="massageClickSub(this, event)">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">China</font>
                </font>
              </a>
            </li>
            <li class="course4" data-value="4">
              <a href="javascript:;" onclick="massageClickSub(this, event)">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Korea</font>
                </font>
              </a>
            </li>
          </ul>
        </div>
        <div class="shop-title pre_list_status" style="">
          <h3>
            <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">Recommended shop</font>
            </font>
          </h3>
          <div class="etc">
            <span>
              <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">advertisement</font>
              </font>
            </span>
            <button type="button" class="tool-tip question" data-toggle="tooltip" data-html="true" title="" data-original-title="프리미엄 광고 영역입니다.">
              <span class="blind">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">detailed description</font>
                </font>
              </span>
            </button>
          </div>
        </div>
       <div class="row">
         <div class="col-12 col-lg-6"> <?php include('inc/service-box.php') ?> </div>
         <div class="col-12 col-lg-6"> <?php include('inc/service-box.php') ?> </div>
         <div class="col-12 col-lg-6"> <?php include('inc/service-box.php') ?> </div>
         <div class="col-12 col-lg-6"> <?php include('inc/service-box.php') ?> </div>
         <div class="col-12 col-lg-6"> <?php include('inc/service-box.php') ?> </div>
         <div class="col-12 col-lg-6"> <?php include('inc/service-box.php') ?> </div>
       </div>
      </article>

      <article class="shop-area anotherlist">
        <div class="shop-title ano_list_status">
          <h3>
            <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">around me</font>
            </font>
          </h3>
        </div>
        <div class="row">
         <div class="col-12 col-lg-6"> <?php include('inc/service-box.php') ?> </div>
         <div class="col-12 col-lg-6"> <?php include('inc/service-box.php') ?> </div>

       </div>
        
      </article>
      <!--<div id="searcharound"></div>-->
      <!-- 찜한 샵, 최근 본 샵 -->
      <aside class="latest-view-shop" id="choicerelated" style="top: 90px;">
        <div class="latest-view-box heart">
          <h2 class="latest-title">
            <a href="/pages/_5_service/service_choice.php">
              <span>
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Favorite Shop </font>
                </font>
              </span>
              <span class="latest-view-count">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">0</font>
                </font>
              </span>
            </a>
          </h2>
          <!-- 최대 15개 까지 -->
          <button class="latest-view-toggle" data-toggle="collapse" data-target="#heartShopCollapase" aria-expanded="false" aria-controls="heartShopCollapase">
            <span class="blind">
              <font style="vertical-align: inherit;">
                <font style="vertical-align: inherit;">Favorite Shop toggle button</font>
              </font>
            </span>
          </button>
          <div class="latest-view-list-box choicebox">
            <ul id="heartShopCollapase" class="latest-view-list choicelist collapse">
              <!-- 데이터가 없을 경우 --->
              <!-- <li class="data-none">찜한 샵이<br>없습니다.</li> -->
              <li class="data-none">
                <font style="vertical-align: inherit;"></font><br>
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">There </font>
                  <font style="vertical-align: inherit;">are </font>
                  <font style="vertical-align: inherit;">no </font>
                  <font style="vertical-align: inherit;">favorite shops </font>
                  <font style="vertical-align: inherit;">.</font>
                </font>
              </li>
            </ul>
          </div>
        </div>
        <div class="latest-view-box">
          <h2 class="latest-title">
            <a href="/pages/_5_service/service_latest.php">
              <span>
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">Recently Viewed Shops </font>
                </font>
              </span>
              <span class="latest-view-count">
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">0</font>
                </font>
              </span>
            </a>
          </h2>
          <!-- 최대 15개 까지 -->
          <div class="latest-view-list-box latedbox">
            <ul class="latest-view-list latedlist">
              <!-- 데이터가 없을 경우 --->
              <!-- <li class="data-none">최근 본 샵이<br>없습니다.</li> -->
              <li class="data-none">
                <font style="vertical-align: inherit;"></font><br>
                <font style="vertical-align: inherit;">
                  <font style="vertical-align: inherit;">There </font>
                  <font style="vertical-align: inherit;">are </font>
                  <font style="vertical-align: inherit;">no </font>
                  <font style="vertical-align: inherit;">recently viewed shops </font>
                  <font style="vertical-align: inherit;">.</font>
                </font>
              </li>
            </ul>
          </div>
        </div>
        <button class="latest-toggle" onclick="latestSideToggleButton(event)">
          <span class="blind">
            <font style="vertical-align: inherit;">
              <font style="vertical-align: inherit;">right toggle button</font>
            </font>
          </span>
        </button>
      </aside>

    </section>
  </div>
</div>
<?php include('inc/footer.php') ?>