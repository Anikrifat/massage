</div>
<footer>
    <div class="con-wrap">
        <nav class="foot-list text-center">
            <ul class="mx-auto">
                <li>
                    <button onclick="modalTermsUse()">Terms of use</button>
                </li>
                <li>
                    <button onclick="modalTermsPersonInfo()">Use of personal information collection</button>
                </li>
                <li>
                    <button onclick="modalTermsLocationInfo()">Location Terms of Use</button>
                </li>
                <li>
                    <button onclick="modalStoreApplication()">Apply for store</button>
                </li>
                <li>
                    <a href="http://mtpartner.msgtong.com/"><strong>Affiliate Manager Page</strong></a>
                </li>
            </ul>
        </nav>

        <p class="text-center">&copy; Developed by <a href="https://spinnertech.dev" target="_blank" style="color: blue;">Spinner Tech</a></p>
        <!-- //foot-list -->
        <!-- <ul class="foot-info">
            <li>
                <strong>LS Innovation </strong>
            </li>
            <li>
                <p class="tit">Business number: </p>
                <span class="text">789-87-00647</span>
            </li>
            <li>
                <p class="tit">CEO: </p>
                <span class="text">Sang-mi</span>
            </li>
            <li>
                <p class="tit">Mail-order sales: </p>
                <span class="text">Seoul Gangseo-2551</span>
            </li>
            <li>
                <p class="tit">Email: </p>
                <span class="text">help@msgtong.com</span>
            </li>
            <li>
                <p class="tit">Address: </p>
                <address class="text"> Room 901, 242, Gonghang-daero, Gangseo-gu, Seoul <span
                        class="m-block">(Magok-dong, Open M Tower II)</span></address>
            </li>
            <li>
                <p class="tit">Customer Center</p>
                <a href="tel:1600-3532" class="text">: 1600-3532</a>
            </li>
            <li>
                <p class="tit">Operating hours: </p>
                <p class="text"><span>Weekdays 9:00am - 5:30pm / Closed on weekends and holidays</span> <span
                        class="m-block">(Lunch break 12pm-1pm )</span></p>
            </li>
        </ul> -->
        <!-- <p class="copyright">ⓒ 2019 MATONG MASSAGE & AESTETIC INC. <span class="m-block">All Rights Reserved.</span></p> -->
    </div>
    <!-- //foot-info -->
    <button class="go-top" onclick="goTop()">
        <span class="area">
            <i class="icon-chevron-up"></i>
        </span>
    </button>
    <!-- // go top btn -->
</footer>










<script src="https://maps.google.com/maps/api/js?key=AIzaSyCOBtzugWMp3JULwZgq5PCF_tkdVfcY2U8"></script>
<script type="text/javascript"
    src="../openapi.map.naver.com/openapi/v3/maps1009.js?ncpClientId=esez1prm2r&amp;submodules=geocoder"></script>

<script src="js/jquery-3.5.1.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>
<script src="js/lib.min.js"></script>
<script src="js/range.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>
<!--
<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=16123429c8f610335ed178dedbb330c7&libraries=services"></script>
-->
<script type="text/javascript" src="../openapi.map.naver.com/openapi/v3/maps1c6e.js?ncpClientId=esez1prm2r"></script>
<script src="../developers.kakao.com/sdk/js/kakao.min.js"></script>
<script type="text/javascript" src="js/custom/common170b.js?vDate=1628527751" charset="utf-8"></script>
<script src="js/base/common.js"></script>
<script src="js/base/autocomplete.js"></script>
<script src="js/lazysizes.min.js" async=""></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="../cdn.jsdelivr.net/npm/handlebars%40latest/dist/handlebars.js"></script>
<script type="text/javascript" src="../cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="../cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script>
    var swiper = new Swiper(".mySwiper4", {
        loop: true,
        slidesPerView: 1,
        autoplay: {
          delay: 2500,
          disableOnInteraction: false,
        }
      });
      var swiper = new Swiper(".mySwiper", {
        loop: true,
        spaceBetween: 10,
        slidesPerView: 6,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
      });
      var swiper2 = new Swiper(".mySwiper2", {
        loop: true,
        spaceBetween: 10,
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        thumbs: {
          swiper: swiper,
        },
      });
      var swiper3 = new Swiper(".mySwiper3", {
        slidesPerView: 1,
        spaceBetween: 10,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 50,
          },
        },
      });
      $(function() {
  var $tabButtonItem = $('#tab-button li'),
      $tabSelect = $('#tab-select'),
      $tabContents = $('.tab-contents'),
      activeClass = 'is-active';

  $tabButtonItem.first().addClass(activeClass);
  $tabContents.not(':first').hide();

  $tabButtonItem.find('a').on('click', function(e) {
    var target = $(this).attr('href');

    $tabButtonItem.removeClass(activeClass);
    $(this).parent().addClass(activeClass);
    $tabSelect.val(target);
    $tabContents.hide();
    $(target).show();
    e.preventDefault();
  });

  $tabSelect.on('change', function() {
    var target = $(this).val(),
        targetSelectNum = $(this).prop('selectedIndex');

    $tabButtonItem.removeClass(activeClass);
    $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
    $tabContents.hide();
    $(target).show();
  });
});
    </script>
<!-- modal area -->
<div class="modal-area">
    <!-- modal : 이용약관 -->
    <div id="modalTermsUse" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalTermsUseLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="modalTermsUseLabel">마통 이용약관</h3>
                </div>
                <!-- // modal header -->
                <div class="modal-body">
                    <div class="term-area" id="term_content1">
                        <p>
                            ※ These terms and conditions include the consent to receive advertising information for
                            commercial purposes.<br><br>

                            <b>Chapter 1 General Rules</b><br><br>

                            <b>Article 1 (Purpose)</b><br>
                            These Terms and Conditions (hereinafter referred to as “Terms”) apply to the use of services
                            (hereinafter referred to as “Services”) provided by the Company between LS Innovation
                            (hereinafter referred to as “Company”) and customers (hereinafter referred to as “Members”).
                            with member
                            The purpose is to prescribe basic matters such as rights, obligations and responsibilities
                            between companies, conditions of use and procedures.<br><br>

                            <b>Article 2 (Definition of Terms)</b><br>
                            Definitions of terms used in these Terms and Conditions are as follows.<br><br>

                            (1) “Service”: refers to the online platform service for reservation, payment, information
                            provision, and recommendation related to management shop information provided by the
                            company. The service is implemented in the device or terminal (PC, TV, portable terminal)
                            (including but not limited to various wired and wireless devices such as, etc.) refers to
                            all services such as Matong-related web and apps,
                            Including cases provided to users through programs or services. The service continues to
                            change and grow together as the management shop culture changes and technology develops.<br>
                            (2) “Member”: A customer who accesses the service, concludes a use contract with the company
                            in accordance with these terms and conditions, uses the service provided by the company, and
                            does not create a member account (ID/PW) (aka non-member)
                            customers) are also included.<br>
                            (3) “Seller”: It means a person who sells his/her own products, services, and services using
                            the services provided by the company, and refers to a person who receives reservations/sales
                            agency, advertising services, etc. from the company.<br>
                            (4) “Post”: Refers to codes (including URLs), texts, voices, sounds, images (including
                            videos), images (including photos), files, etc. posted or registered by members and
                            customers on the service.<br>
                            (5) “Coupon”: When a member uses the service, it refers to a discount coupon or preferential
                            ticket (regardless of online, mobile, offline, etc.) that allows the member to receive a
                            discount for the amount or percentage indicated when using the service. type of coupon and
                            Contents may vary according to company policy.<br>
                            (6) “Points”: The company provides digitized virtual data that can be used for payment, such
                            as products within the service, provided by the company to the members for the benefit of
                            the member’s service use or the convenience of using the service.
                            Says. The specific method of use, its name (eg mileage, etc.) and the possibility of cash
                            refund may vary depending on the company's policy.<br><br>

                            <b>Article 3 (Effect of Terms and Conditions and Changes)</b><br>
                            ① These terms and conditions take effect for all members who wish to use the service.<br>
                            ② These terms and conditions can be read by members when they sign up for the service, and
                            the company posts them on the company website or app so that members can read the terms and
                            conditions at any time they want.<br>
                            ③ The Company shall comply with 『Act on the Regulation of Terms and Conditions』, 『Act on
                            Consumer Protection in Electronic Commerce, etc.』, 『Promotion of Information and
                            Communications Network Utilization and Information Protection (hereinafter referred to as
                            “Information and Communications Network Act”)』, 『Basic Consumer Act』, 『 electronic documents
                            and
                            These Terms and Conditions may be amended to the extent that it does not violate the
                            relevant laws such as the ‘Basic Act on Electronic Transactions’.<br>
                            ④ If the company changes the terms and conditions, the date of application and the reason
                            for the change shall be specified and notified 7 days prior to the date of application.
                            However, in the case of a change of the terms and conditions unfavorable to the member, from
                            30 days prior to the date of application,
                            Notify and notify members individually by e-mail, etc. However, if individual notification
                            is difficult due to member's failure to enter contact information or modification after
                            change, the notification is considered as an individual notification.<br>
                            ⑤ Even though the company notifies or notifies the amendment of the terms and conditions in
                            accordance with Paragraph 4, and the member does not indicate his/her intention to reject
                            the terms and conditions by the effective date of the change, the member will be deemed to
                            have agreed to the change of the terms and conditions.
                            If the member does not expressly refuse to change the terms and conditions, it is considered
                            that the member agrees to the revised terms.<br>
                            ⑥ If a member does not agree to the changed terms and conditions, he/she may stop using the
                            service and terminate the contract.<br>
                            ⑦ Members must fulfill their duty of care in relation to changes to the terms and
                            conditions, and the company is not responsible for any damage to members caused by the site
                            of the changed terms and conditions.<br><br>

                            <b>Article 4 (Relationship with other rules and related laws and regulations)</b><br>
                            ① Matters not stipulated in these Terms and Conditions or the individual terms and
                            conditions apply to the 『Telecommunications Business Act』, 『The Basic Act of Electronic
                            Documents and Electronic Transactions』, 『Act on Promotion of Information and Communications
                            Network Utilization and Information Protection, etc.』, 『Consumer Protection in Electronic
                            Commerce, etc.
                            Act], the 「Personal Information Protection Act」 and other related laws and the detailed
                            service usage guidelines set by the company. In addition, the company's liability limits set
                            forth in these Terms and Conditions are to the maximum extent permitted by the relevant laws
                            and regulations.
                            applied within.<br>
                            ② If necessary, the company may set individual terms and conditions or operating principles
                            for individual items within the service.
                            Applies.<br><br>

                            <b>Chapter 2 Conclusion of the Use Agreement</b><br><br>

                            <b>Article 5 (Establishment of contract of use)</b><br>
                            In the contract of use, the person who wants to become a member (hereinafter referred to as
                            the “applicant for membership”) agrees to the terms and conditions, fills out member
                            information according to the registration form set by the company, applies for membership,
                            and the company approves the application.
                            It is fastened.<br><br>

                            <b>Article 6 (Reservation and Rejection of Contract of Use)</b><br>
                            ① The company may not approve the application for each of the following subparagraphs or
                            terminate the use contract afterwards.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(1) In case of false information or omissions or errors in the
                            application form <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(2) In case of using someone else's name, e-mail, contact
                            information, etc.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(3) In case the application is made for the purpose of violating
                            related laws or undermining social order or morals<br>
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(5) In case approval is not possible due to reasons attributable to
                            the member<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(6) In case the phone number or e-mail information is the same as
                            that of an already registered member<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(7) If you want to use this service for illegal purposes or profit
                            &nbsp;&nbsp;&nbsp;&nbsp;(8) When it is confirmed that the application is in violation of
                            these terms and conditions or is an illegal or unreasonable application, and the company
                            deems it necessary based on a reasonable judgment<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(9) When a child under the age of 14 applies<br>
                            ② In the application pursuant to Paragraph 1, the company may request identity verification
                            through mobile phone number verification or real name verification through a specialized
                            institution.<br>
                            ③ The company may withhold the approval of membership registration in the following
                            cases.<br>
                            <br><br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(2) When it is determined that there is a technical problem in
                            providing the service<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(3) When the company deems it financially and technically
                            necessary<br>
                            &nbsp;&nbsp;&nbsp;<br>
                            ④ In the event that the application for membership is not accepted or withheld in accordance
                            with Paragraphs 1 and 3, the company notifies the applicant of refusal or reservation of
                            acceptance in accordance with the method determined by the company.<br>
                            ⑤ The time of establishment of the contract of use shall be the time when the company
                            notifies the fact of ‘subscription complete’ on the application procedure or separately
                            notifies it.<br><br>

                            <b>Article 7 (Management of Member Information)</b><br>
                            ① Members can view, change, and modify personal information through the information
                            correction function provided in the website or app or through the customer center.<br>
                            ② If the registered contact information is changed, the member must change it in accordance
                            with Paragraph 1 to maintain the up-to-dateness of the member information, and all
                            disadvantages that may arise from not changing it shall be borne by the member.<br><br>

                            <b>Article 8 (Responsibility for Account Information Management)</b><br>
                            ① Members are responsible for managing account information such as ID and password, and
                            members cannot transfer or lend account information to others.<br>
                            ② The company is not responsible for any loss or damage caused by leakage, transfer, rental,
                            sharing, etc. of account information, except for reasons attributable to the company.<br>
                            ③ If a member recognizes that a third party is using his or her account (including rental),
                            he/she must immediately take measures such as correcting the password and notify the company
                            of this. If the member fails to notify in accordance with this paragraph,
                            Members are responsible for any disadvantages that occur.<br><br>

                            <b>Article 9 (Collection and Protection of Member Information)</b><br>
                            ① In providing services, the company complies with laws related to personal information and
                            collects, uses, stores, and provides member information accordingly.<br>
                            ② In addition to the information directly provided by the member in the process of using the
                            service, the company may collect and use other information or provide it to a third party in
                            accordance with the procedures stipulated in the relevant laws, such as personal information
                            protection. In this case, the company
                            Obtain the necessary consent from the member in accordance with the relevant laws or comply
                            with the procedures set forth in the relevant laws.<br>
                            ③ The company strives to protect members' personal information as stipulated by laws related
                            to personal information protection, and details regarding the company's personal information
                            processing can be checked at any time through the personal information processing policy.
                            Yes.<br>
                            ④ The company's privacy policy does not apply to linked sites other than the company's
                            official site or app. Regarding the processing of personal information of third parties that
                            provide linked sites and services, those sites and third parties
                            The member is responsible for checking the privacy policy, and the company is not
                            responsible for it.<br><br>
                            <b>Chapter 3 Service Use</b><br><br>

                            <b>Article 10 (Start of use of service)</b><br>
                            The member can use the service from the time the company approves the application for use,
                            and the company provides the service according to these terms and conditions from the time
                            the company approves the application.<br><br>

                            <b>Article 11 (Hours of service use)</b><br>
                            ① In principle, the service is provided 24 hours a day, 7 days a week, 7 days a week, unless
                            there is a special reason for the company's business or technology. The company may divide
                            the service into a certain range and specify the available time for each range separately.
                            Yes.<br>
                            ② The company may conduct regular or occasional inspections if necessary for the provision
                            of services, and the period of regular inspections or occasional inspections is as announced
                            on the service provision screen.<br><br>

                            <b>Article 12 (Content of Service)</b><br>
                            ① Service refers to the management shop-related online platform service provided by the
                            company to its members as defined in Article 2, No. 1. The service is not limited to the
                            products currently provided, and may be further developed in the future or
                            It can be added, changed, or modified through partnership with the company.<br>
                            ② The company shall not be liable for any other significant reasons, such as difficulties in
                            providing smooth services due to membership reduction, deterioration of profitability, the
                            need to switch to next-generation services due to technological advances, changes in company
                            policies related to service provision, etc.
                            In some cases, all or part of the services provided may be changed or discontinued depending
                            on operational and technical needs.<br>
                            ③ If there is a change or service interruption in the contents, usage method, and usage time
                            of the service, the contents, reason and date of the service to be changed or discontinued
                            shall be posted on the company website or service prior to the change or suspension.
                            It is notified in advance with a period of 30 days in a way that members can fully
                            recognize, such as the "Notice" screen.<br>
                            ④ Due to the nature of the service provided free of charge, when all or part of the service
                            in accordance with this Article is terminated, no separate compensation will be made to the
                            member unless specifically stipulated in the relevant laws and regulations.<br>
                            ⑤ Individual guidance on service use, information on products, etc., precautions when making
                            a reservation, cancellation/refund policy, etc. are provided through individual service
                            usage guides/introductions.<br>
                            ⑥ Members must fully understand the information and introduction of the preceding paragraph
                            before using the service. As a mail order broker, the company is not a party to mail order
                            sales, and the seller
                            There may be cases where a separate cancellation/refund policy may be operated, so the
                            member must check the contents when using products or making a reservation. The company will
                            be responsible for any damage caused by the member's failure to properly understand this
                            information.
                            No responsibility.<br><br>

                            <b>Article 13 (Change and Suspension of Service)</b><br>
                            ① The company may notify the member of the contents of the service to be changed and the
                            date of provision in the manner stipulated in Article 19 of these Terms and Conditions, and
                            change the service.<br>
                            ② The company may restrict or suspend all or part of the service if it falls under any of
                            the following subparagraphs.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(1) When a member interferes with the company's business
                            activities<br>
                            <br><br>
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(4) In case the company deems it inappropriate to continue providing
                            services due to other serious reasons<br>
                            <br>
                            ③ In case of service interruption pursuant to Paragraph 2, the company notifies the member
                            in the manner set forth in Article 21. However, service interruption due to reasons beyond
                            the company's control (disruption without intention or negligence of the operator, system
                            down
                            etc.), this is not the case if prior notice is not possible.<br>
                            ④ The company is not responsible for any problems arising from the change or suspension of
                            the service according to paragraph 2.<br><br>

                            <b>Article 14 (Provision of information and publication of advertisements)</b><br>
                            ① A part of the service investment base on which the company can provide services to members
                            comes from revenues from advertisements. As a result, the member shall be responsible for
                            posting advertisements displayed on the service screen when using the service.
                            I agree.<br>
                            ② The company provides customized advertisements using standard log information such as
                            language, cookie and device information, IP address, browser type, operating system and
                            request date, as well as member's post content and search content. Therefore
                            For more details, please refer to the "Privacy Policy".<br>
                            ③ When a member uses advertisements posted on the service or other products, etc. for the
                            advertiser's promotional activities through the service, this is entirely a legal
                            relationship between the member and the advertiser.
                            All issues such as disputes between advertisers must be resolved directly between the member
                            and the advertiser, and the company does not take any responsibility in this regard.<br>
                            ④ The company may send various marketing information that is deemed necessary while using
                            the service of the member through the personal information collected from the member using
                            SMS (LMS), smartphone notification (push notification), e-mail, etc.
                            and the member agrees to this. However, members may withdraw their consent to receive
                            marketing at any time in accordance with relevant laws, except for transaction-related
                            information and answers to customer inquiries, and in this case, the company will
                            Stop providing information (however, there may be a time lag in system reflection).<br><br>

                            <b>Article 15 (Right to Post)</b><br>
                            ① The copyright of the post belongs to the member. However, the company may use, edit, and
                            modify the posts written by members for the purpose of posting, delivering, and sharing, and
                            may use other services of the company or linked channels and sales channels.
                            You can publish or utilize it.<br>
                            ② When the company uses posts for purposes other than posting in the service, the publisher
                            of the post must be specified. However, this is not the case for anonymous postings or
                            non-commercial cases where the publisher is unknown.
                            No.<br>
                            ③ Members must not infringe other rights, including intellectual property rights, such as
                            copyrights, of others when writing posts, and the company does not bear any responsibility
                            for them. If a member violates the rights of others
                            If another person files an objection, compensation claim, or deletion request against the
                            company on the grounds of infringement, the company may take necessary measures, and the
                            member is responsible for all costs and damages.
                            I will bear it.<br>
                            ④ The company may delete the posts posted by the member when the member terminates the
                            contract of use and leaves the site or is terminated for a legitimate reason.<br>
                            ⑤ All rights and responsibilities for posts written by members belong to the member who
                            posted them, and the company deletes or deletes posts without prior notice if it is
                            determined that the posts posted or registered by members fall under any of the following
                            subparagraphsTemporary measures can be taken, and the company does not take any
                            responsibility for this.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(1) In case of content that slanders or damages other members or
                            third parties<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(2) In case of distributing or linking content that violates public
                            order and morals<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(3) Content that promotes illegal copying or hacking<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(4) In case of posting commercial advertisements or promotions
                            without prior approval from the company<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(5) In case of requesting money transaction between individuals<br>
                            &nbsp;&nbsp;<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(7) In the case of contents that infringe other rights such as the
                            company's copyright or the copyright of a third party<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(8) In the case of content created by stealing another person's
                            account information or name without permission
                            &nbsp;&nbsp;&nbsp;&nbsp;(9) When the company determines that the content of personal
                            political or religious views does not fit the nature of the service<br>
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(11) In case of violating the posting principles stipulated by the
                            company or inconsistent with the nature of the posting position <br>
                            <br>
                            &nbsp;&nbsp;&nbsp;<br>
                            ⑥ If a member's post contains content that violates related laws such as the Information and
                            Communications Network Act and the Copyright Act, the right holder may request the
                            suspension or deletion of the post in accordance with the procedures stipulated by the
                            relevant laws, and the company shall comply with the relevant laws.
                            You must act accordingly.<br>
                            ⑦ Even if there is no request from the right holder pursuant to Paragraph 2 of this Article,
                            if there is a reason for a violation of rights or if it violates other company policies and
                            related laws, the company may take temporary measures, etc.
                            You can take it.<br>
                            ⑧ The detailed procedure under this Article is subject to the post-discontinuation request
                            service set by the company within the scope stipulated by the Information and Communications
                            Network Act and the Copyright Act.<br>
                            - Request to take down post: lsinnovation@naver.com<br><br>

                            <b>Article 16 (Return of Rights)</b><br>
                            ① The copyright and intellectual property rights for the service belong to the company.
                            However, postings and works provided under the partnership agreement are excluded.<br>
                            ② All trademarks, service marks, logos, etc. related to the services provided by the
                            company, such as the design of the services provided by the company, texts, scripts,
                            graphics, and member-to-member transmission functions
                            Copyrights and other intellectual property rights are owned by the company or the company
                            has the ownership or right to use it in accordance with the laws of Korea and foreign
                            countries.<br>
                            ③ The member does not own the service or the copyright on the service due to these terms of
                            use, but rather gets permission to use the service from the company. The service is provided
                            only for information acquisition or personal use
                            It is available to members in the form.<br>
                            ④ Members use, copy, and distribute the member status information obtained through the
                            service for commercial purposes, except for expressly permitted texts, scripts, and graphics
                            created by the company between members.
                            You cannot copy or distribute transmission functions, etc.<br>
                            ⑤ In relation to the service, the company grants the member only the right to use the
                            account, ID, content, etc. according to the conditions of use set by the company, and the
                            member may transfer, sell, provide collateral, etc.
                            None.<br>
                            ⑥ Members shall use the information obtained in the course of using the service for
                            commercial purposes or use it for a third party by copying, transmitting, publishing,
                            distributing, broadcasting, editing, reprocessing, etc., without prior consent of the
                            company.
                            You shouldn't.<br><br>

                            <b>Article 17 (Points)</b><br>
                            ① Points are granted to members according to the company's policy, and matters related to
                            the accumulation criteria, usage method, expiration date and restrictions for each point are
                            separately announced or notified. However, there is no separate notice about the expiry date
                            of points.
                            In the case of 6 months.<br>
                            ② Points will automatically expire if they are not used during the period of use or if there
                            is a reason for membership withdrawal or loss of qualifications. If points are expended due
                            to membership withdrawal or loss of qualifications, the expired points will not be restored
                            even if you rejoin.
                            not.<br>
                            ③ Members cannot transfer points to a third party except as otherwise specified by the
                            company. If it is confirmed that the member has obtained/used points in a method not
                            approved by the company, the company will use the points
                            You can cancel your reservation request or suspend or terminate your membership.<br>
                            ④ Point-related company policies may change depending on the company's business policies. In
                            the case of a change unfavorable to the member, it is notified or notified in accordance
                            with the provisions of Article 3, and it is deemed to have been agreed upon when continuing
                            to use the service.
                            considered.<br><br>

                            <b>Article 18 (Coupon)</b><br>
                            ① Coupons are coupons issued by the company for a fee or free of charge, and can be
                            classified according to the subject of issuance, the issuance route, and the subject of use.
                            It is displayed on the coupon or service screen. The type and content of coupons and whether
                            or not they are issued may vary depending on the company's business policy.<br>
                            ② Coupons cannot be withdrawn in cash, and will expire when the period of use indicated on
                            the coupon expires or the contract of use is terminated.<br>
                            ③ If the reservation transaction is canceled, whether the coupon used for the reservation
                            will be returned or not will be determined according to the company's policy, and details
                            will be provided through a wired notice or reservation service screen.<br>
                            ④ Members cannot transfer coupons to third parties or other IDs, and cannot transact for a
                            fee or convert them into cash, except as otherwise specified by the company. If the member
                            is not approved by the company
                            If it is confirmed that coupons have been obtained/used by fraudulent methods, the company
                            may cancel the reservation application using the coupon or suspend or terminate
                            membership.<br>
                            ⑤ Coupon-related company policies may change depending on the company's business policies.
                            In the case of a change unfavorable to the member, it is notified or notified in accordance
                            with the provisions of Article 3, and it is deemed to have been agreed upon when continuing
                            to use the service.
                            considered.<br><br>
                            <b>Chapter 4 Obligations of Contracting Parties</b><br><br>

                            <b>Article 19 (Obligations of the Company)</b><br>
                            ① The company does not engage in acts prohibited by the relevant laws and these terms and
                            conditions or contrary to good morals and strives to provide continuous and stable
                            service.<br>
                            ② The company must have a security system in place to protect personal information so that
                            members can use the service safely, disclose and comply with the personal information
                            processing policy, disclose the member's personal information to a third party without the
                            person's consent,
                            We do not distribute it, we try to protect it.<br>
                            ③ If the company recognizes that the opinions or complaints raised by members in relation to
                            the use of the service are justified, the company must take necessary measures.<br>
                            ④ In the event of damage to the member due to the service provided by the company, the
                            company is liable only if the damage is caused by the intention or negligence of the
                            company, and the scope of liability is limited to normal damage.
                            Han.<br><br>

                            <b>Article 20 (Responsibilities of Members)</b><br>
                            ① Members must comply with other related laws and regulations, the provisions of these Terms
                            and Conditions, notices on use and service, notices from the company, etc., and must not
                            engage in any other acts that interfere with the business of the company.
                            ② Members cannot transfer or donate the right to use the service or other status in the
                            service use contract to another person, and cannot provide it as collateral.<br>
                            ③ Members shall not engage in any of the following acts in connection with the use of the
                            service.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(1) Registering false information when applying for or changing
                            services<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(2) Using the service unfairly by stealing another member's ID and
                            password or stealing information<br>
                            <br>
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(5) Changing the information posted by the company<br>
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(7) Acts that infringe intellectual property rights such as
                            copyrights of the company and other third parties<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(8) Acts that damage the reputation of the company or other third
                            parties or interfere with business<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(9) Acts of disclosing or posting obscene or violent messages,
                            images, voices, or other information contrary to public order and morals<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(10) Act of using the service for profit without the consent of the
                            company<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(11) Posting articles or sending e-mails by impersonating or
                            impersonating an employee of the company or the administrator of the service, or by stealing
                            the name of another person<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;12
                            &nbsp;&nbsp;&nbsp;&nbsp;(13) The act of rewriting the software provided by the company or
                            copying, disassembling, imitating, or otherwise modifying the service through reverse
                            engineering, decompilation, disassembly and any other processing <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(14) Using the service in a manner different from normal usage, such
                            as using an automatic connection program, etc.
                            &nbsp;&nbsp;&nbsp;&nbsp;(15) Acts of collecting, storing, and disclosing other members'
                            personal information without their consent<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(16) Any other illegal or violation of company regulations<br>
                            ④ The company may delete or temporarily delete the relevant postings, etc., if the member
                            performs the acts in paragraph 1, restrict the use of the service, or unilaterally terminate
                            this contract.<br>
                            ⑤ If there is a change in member information or account information, the member must change
                            it immediately in accordance with Article 7, and also manage the password thoroughly. due to
                            negligence in management, illegal use, etc. attributable to the member.
                            The member is responsible for all the results that occur, and the company does not bear any
                            responsibility for it.<br>
                            ⑥ If a member who is a minor under the Civil Act uses the paid service, the member who is a
                            minor must obtain the consent of a legal representative before payment, and children under
                            the age of 14 cannot use this service.<br>
                            ⑦ Members may not engage in sales activities to sell products using the service except in
                            cases officially recognized by the company.
                            Unlawful distribution is prohibited. The company is not responsible for the results and
                            losses of business activities caused in violation of this and legal actions such as arrest
                            by related organizations, etc.
                            We are obligated to compensate the company for damages.<br><br>
                            <b>Article 21 (Notification to Members)</b><br>
                            ① If the company notifies the member, unless otherwise provided in these terms and
                            conditions, it can be done by e-mail written by the member.<br>
                            ② In case of notice to many unspecified members, the company may substitute individual
                            notice by posting it on the service bulletin board.<br>

                            <b>Article 22 (Termination of Service Use)</b><br>
                            ① When a member wishes to terminate the contract of use, he or she may apply for
                            cancellation according to the cancellation method guided through the site or app.<br>
                            ② When the application for cancellation of registration is received, the company must cancel
                            the member's use of the service at the desired time.<br>
                            ③ When a member terminates the contract, the member's personal information will be deleted
                            immediately upon termination, except in cases where the company may retain member
                            information in accordance with relevant laws and personal information processing
                            policies.<br><br>

                            <b>Article 23 (Restriction on Service Use)</b><br>
                            ① As stipulated in the use restriction policy, if a member violates the obligations under
                            these terms and conditions or interferes with the normal operation of the service, the
                            company restricts the use of the service by warning, temporary suspension, or permanent
                            suspension of use, etc.
                            You can cancel the contract of use. However, if it is confirmed that the member has the
                            cause under Article 6 Paragraph 1 or the member engages in, encourages or aids in illegal
                            acts in relation to the use of the service, immediate permanent suspension of use or
                            contract of use
                            You can cancel.<br>
                            ② If a member has not used the service for more than one year continuously, the company may
                            take necessary measures in accordance with the Information and Communications Network Act,
                            and may restrict the use for the protection of member information and efficiency of
                            operation.<br>
                            ③ In the event of restricting the use of the service or terminating the contract according
                            to this Article, the Company will notify the member in accordance with Article 19.<br>
                            ④ A member may file an objection in accordance with the procedures set by the company
                            regarding the suspension of service use or other restrictions related to the use of the
                            service pursuant to this Article
                            Resume use.<br><br>


                            <b>Chapter 5 Others</b><br><br>

                            <b>Article 24 (Compensation for Damages)</b><br>
                            ① If a member violates the provisions of these terms and conditions and causes damage to the
                            company, the member who violates these terms and conditions must compensate the company for
                            all damages.<br>
                            ② In the event that the company receives various objections, including claims for damages or
                            lawsuits, from a third party other than the member due to illegal acts or violations of
                            these terms and conditions performed by the member in the process of using the service, the
                            member
                            The company must be exempted from liability and expenses, and if the company is not
                            exempted, the member must compensate for all damages incurred by the company.<br><br>

                            <b>Article 25 (Exemption Matters)</b><br>
                            ① If the company cannot provide the service due to a natural disaster or force majeure
                            equivalent thereto, the company is exempted from responsibility for the provision of the
                            service.<br>
                            ② The company is not responsible for any obstacles to the use of the service due to reasons
                            attributable to the member.<br>
                            ③ The company is not responsible for the loss of revenue expected by the member using the
                            service, and the company is not responsible for any other damage caused by data obtained
                            through the service. The company posted by the member
                            We are not responsible for the accuracy of posts (including reviews and management shop
                            evaluations).<br>
                            ④ The company has no obligation to intervene in disputes between members or between members
                            and a third party through the service, and is not responsible for compensation for damage
                            caused by this.<br>
                            ⑤ Products, etc. are managed and operated under the responsibility of the seller, and the
                            company, as a mail-order sales broker, is responsible for defects or insolvency of products,
                            etc., except for problems in service operation, to the seller, and the company does not bear
                            any responsibility.
                            not.<br>
                            ⑥ The company has no obligation to monitor the contents and quality of products or services
                            advertised by third parties through the screens within the service or through the linked
                            website, and does not take any responsibility for it.<br>
                            ⑦ The company shall not be held liable for any damages arising from the following matters
                            unless there is intentional or gross negligence of the company, its executives and
                            employees, and the company's agent.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(1) Damage caused by false or inaccurate member information <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(2) Personal damage incurred in the process of accessing the service
                            and using the service
                            &nbsp;&nbsp;&nbsp;&nbsp;(3) Any illegal access by a third party to the server or damage
                            resulting from the illegal use of the server<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(4) Damages resulting from any unlawful interference or interruption
                            of transmission by a third party to or from the server<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(5) Damage caused by any virus, spyware and other malicious programs
                            that are illegally transmitted, distributed, or caused to be transmitted or distributed by a
                            third party using the service<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(6) Damages caused by errors, omissions, omissions, and destruction
                            of transmitted data<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;(7) Various civil and criminal liabilities for defamation and other
                            illegal acts occurring in the process of registering member status information between
                            members and using services<br><br>

                            <b>Article 26 (Dispute Mediation and Jurisdiction Court)</b><br>
                            The laws of the Republic of Korea apply to these terms and conditions and disputes between
                            the company and members, and the court having jurisdiction over the address of the company
                            shall be the competent court.<br><br>


                            <b>* Supplement</b><br>
                            First effective date: November 1, 2016<br>
                            Enactment Date: February 12, 2020<br>
                            Effective Date: 00/00, 2020
                        </p>
                    </div>
                </div>
                <!-- //modal body -->
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <!-- modal : 개인정보 취급방침 -->
    <div id="modalTermsPersonInfo" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="modalTermsPersonInfoLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="modalTermsPersonInfoLabel">Maton personal information collection and use
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="term-area" id="term_content3">
                        <p>
                            LS Innovation values ​​users' personal information and strives to comply with personal
                            information protection laws such as 「Act on Promotion of Information and Communications
                            Network Utilization and Information Protection, Etc.」 and 「Personal Information Protection
                            Act」.<br><br>

                            The company notifies the members of the measures to protect the company's personal
                            information, including the processing of users' personal information, through the personal
                            information processing policy, and regulates the rights and obligations between the company
                            and the members to protect the members' right to self-determination of personal information
                            Guaranteed.<br><br>

                            <b>Purpose of collection and use consent</b><br>

                            Service subscription, contract fulfillment and service provision,
                            reservation/payment/interested product details, payment billing,
                            consultation/complaint/complaint handling, notification/guidelines, statistics/analysis of
                            product/service usage performance information, product/service improvement and
                            Recommendation, prevention of illegal and illegal use, compliance with personal information
                            validity period<br><br>

                            <b>Items</b><br>

                            -When using the service: ID (e-mail), password, nickname, device ID, mobile phone number,
                            service use record, IP address, access log, cookie, advertisement identifier, terminal OS
                            and version, terminal model name,
                            Browser version<br>
                            - For reservation and payment: name and mobile phone number of the person making the
                            reservation and payment, reservation details (reservation date and time, payment amount,
                            company name)<br><br>

                            <b>Retention/Use Period</b><br>

                            - Deletion after 7 days upon request for membership withdrawal<br>
                            - If it is necessary to preserve in accordance with the relevant laws and regulations, it is
                            stored until the deadline required by the relevant laws (e.g., 5 years for payment members,
                            3 months for IP)

                        </p>
                    </div>
                </div>
                <!-- //modal body -->
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <!-- modal : Location Information Terms of Use -->
    <div id="modalTermsLocationInfo" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="modalTermsLocationInfoLabel" aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="modalTermsLocationInfoLabel">Matong Location Information Terms of Use
                    </h3>
                </div>
                <div class="modal-body">
                    <div class="term-area" id="term_content2">
                        <p>
                            <b>Article 1 (Purpose)</b><br><br>
                            These terms and conditions are used in accordance with the rights, obligations and
                            responsibilities of the company, customers, and personal location information subjects when
                            using location-based services (hereinafter referred to as “services”) operated and provided
                            by LS Innovation (hereinafter referred to as the “company”). conditions and
                            The purpose is to prescribe basic matters such as procedures.<br><br>

                            <b>Article 2 (Effect and Change of Terms of Use)</b><br>
                            1. These terms and conditions take effect when the customer or the subject of personal
                            location information who has applied for the service agrees to these terms and registers as
                            a user of the service according to the prescribed procedure set by the company.<br>
                            2. If the applicant selects the "Agree" button on the terms and conditions on a mobile
                            device, PC, etc., it is deemed that they have read and fully understood the contents of
                            these terms and conditions and have agreed to their application.<br>
                            3. The company does not violate relevant laws, such as the Act on the Protection and Use of
                            Location Information, the Content Industry Promotion Act, the Consumer Protection Act in
                            Electronic Commerce, etc., and the Consumer Basic Act Act on the Regulation of Terms
                            You may change these terms and conditions.<br>
                            4. When the company changes the terms and conditions, the changed terms and conditions,
                            reasons, and application date are specified, and the notice is made from 10 days before the
                            effective date to a considerable period after the effective date.
                            From 30 days before the effective date to the effective date, for a considerable period
                            after the effective date, each notice is posted in the service or by sending the member an
                            electronic form (e-mail, SMS, etc.)
                            5. If the company notifies the member in accordance with the preceding paragraph and does
                            not express its intention to reject it by 7 days after the date of application, it is deemed
                            to have approved the revised terms and conditions. If the member does not agree to the
                            revised terms and conditions, the member
                            You can cancel the contract of use.<br><br>

                            <b>Article 3 (Application of Relevant Laws)</b><br>
                            These terms and conditions are applied fairly in accordance with the principle of good
                            faith, and related laws or commercial practices are followed for matters not specified in
                            these terms and conditions.<br><br>

                            <b>Article 4 (Content of Service)</b><br>
                            The company provides the following services by using the location information and status
                            information of the location information collection target provided by the location
                            information service provider.<br>
                            1. Real-time location check of location information collection target<br>
                            2. Provision of information on shopping malls, neighborhood facilities, and businesses close
                            to the user's location<br><br>

                            <b>Article 5 (Notification of Service Content Change)</b><br>
                            1. If the company changes or terminates the service contents, the company may notify the
                            change or termination of the service contents through e-mail to the registered e-mail
                            address.<br>
                            2. In the case of Paragraph 1, when notifying a large number of unspecified persons, the
                            member may be notified through the website and other company notices.<br><br>

                            <b>Article 6 (Restriction and Suspension of Service Use)</b><br>
                            1. The company may restrict or suspend a member's use of the service if the user falls under
                            any of the following subparagraphs.<br>
                            1) When a member intentionally or grossly negligently interferes with the operation of the
                            company service <br>
                            2) In case of unavoidable circumstances due to service facility inspection, repair or
                            construction <br>
                            3) In case the telecommunication service is suspended by the key telecommunication service
                            provider stipulated in the Telecommunications Business Act<br>
                            4) When there is a problem in using the service due to a national emergency, failure of
                            service facilities, or congestion of service use
                            5) In case the company recognizes that it is inappropriate to continue providing services
                            due to other serious reasons<br>
                            2. When the company restricts or suspends the use of the service in accordance with the
                            provisions of the preceding paragraph, it must notify the member of the reason and period of
                            restriction.<br><br>

                            <b>Article 7 (Service usage fee)</b><br>
                            Customers can use this service free of charge. However, communication charges, which are
                            costs incurred when accessing a mobile communication network to check location information,
                            may occur. The communication fee depends on the customer's data usage, the mobile operator's
                            It is subject to change depending on the circumstances.<br><br>

                            <b>Article 8 (Use or Provision of Personal Location Information)</b><br>
                            1. If the company intends to provide services using personal location information, it must
                            obtain the consent of the subject of personal location information after specifying it in
                            the terms of use in advance.<br>
                            2. The rights of members and their legal representatives and the method of exercising them
                            depend on the address of the user at the time of filing, and if there is no address, the
                            district court having jurisdiction over the place of residence shall have exclusive
                            jurisdiction. However, the address or residence of the user at the time of filing
                            If it is unclear or if it is a foreign resident, it is submitted to the competent court
                            under the Civil Procedure Act.<br>
                            3. When the company provides personal location information to a third party designated by
                            the member, the company immediately notifies the person to whom the personal location
                            information is provided, the date of provision and the purpose of provision to the member
                            through the communication terminal device from which the personal location information was
                            collected. only,
                            In the case of any of the following cases, the member will notify in advance to the
                            designated communication terminal device or e-mail address.<br>
                            1) When the communication terminal device that has collected personal location information
                            does not have a text, voice or video reception function <br>
                            2) When the subject of personal location information has requested in advance to notify via
                            online posting, etc.<br><br>

                            <b>Article 9 (Rights of subject of personal location information)</b><br>
                            1. Members may withdraw all or part of their consent to the company at any time to provide
                            location-based services using personal location information and to provide personal location
                            information to third parties. In this case, the company collects personal location
                            information
                            We will destroy data that confirms the use of location information and the fact that it is
                            provided.<br>
                            2. Members may request the company to temporarily suspend the use or provision of personal
                            location information at any time. In this case, the company does not reject the request and
                            takes technical measures for this.<br>
                            3. You can request the company to read or notify the data in each of the subparagraphs
                            below, and if there is an error in the data, you can request the correction. In this case,
                            the company does not reject the request without justifiable reasons.
                            No.<br>
                            1) Data confirming the use and provision of location information to the subject of personal
                            location information<br>
                            2) The reason and content of the personal location information of the subject of personal
                            location information provided to a third party in accordance with the Act on the Protection
                            and Use of Location Information or other laws
                            4. Members may request from the company through the company's prescribed procedures to
                            exercise the rights under paragraphs 1 to 3.<br><br>

                            <b>Article 10 (Rights of Legal Representative)</b><br>
                            1. The company uses the personal location information of children under the age of 14. In
                            the case of providing the service (including the service provided to a third party
                            designated by the subject of personal location information), a child under the age of 14 and
                            their legal representative
                            Consent is required. In this case, the legal representative has all the rights of the member
                            under Article 9.<br>
                            2. If the Company intends to use the personal location information of children under the age
                            of 14, use of location information, or data confirming the provision of such information
                            beyond the scope specified or notified in the Terms of Use or provide it to a third party,
                            that
                            You must obtain the consent of your legal representative. However, the following cases are
                            excluded.<br>
                            1) When data to confirm the use and provision of location information is required for the
                            settlement of charges for location information and location-based service provision <br>
                            2) When providing a specific individual in an unrecognizable form for statistical
                            preparation, academic research, or market research<br><br>

                            <b>Article 11 (Rights of persons obligated to protect children under the age of 8)</b><br>
                            1. The company agrees to the use or provision of personal location information for the
                            protection of the life or body of children under the age of 8 by the person obligated to
                            protect the persons falling under the following circumstances (hereinafter referred to as
                            “children under the age of 8”) In case
                            It is assumed that you have your consent.<br>
                            1) Children under the age of 8<br>
                            2) Incompetent <br>
                            3) Persons with mental disabilities under Article 2 (2) 2 of the Welfare of the Disabled
                            Act, who are severely disabled under Article 2 (2) of the Employment Promotion and
                            Vocational Rehabilitation for the Disabled Act (According to Article 29 of the Welfare Act
                            for the Disabled)
                            Limited to those who have registered the disabled)<br>
                            2. A person responsible for protection who wants to consent to the use or provision of
                            personal location information for the protection of the life or body of children under the
                            age of 8, etc.
                            to.<br>
                            3. The person responsible for protection can exercise all of the rights of the subject of
                            personal location information if they agree to the use or provision of personal location
                            information for children under the age of 8.<br><br>

                            <b>Article 12 (Designation of location information manager)</b><br>
                            1. The company designates and operates a person in a position to take practical
                            responsibility as the location information manager to properly manage and protect location
                            information and to handle complaints from the subject of personal location information.<br>
                            2. The location information manager is the head of the department that provides
                            location-based services, and specific details follow the supplementary provisions of these
                            terms and conditions.<br><br>

                            <b>Article 13 (Scope of compensation)</b><br>
                            1. In the event that the company violates the provisions of Articles 15 to 26 of the Act on
                            the Protection and Use of Location Information, the member may claim damages from the
                            company. In this case, the company willfully or negligently
                            You cannot escape liability unless you prove that you do not.<br>
                            2. If a member violates the provisions of these terms and conditions and causes damage to
                            the company, the company may claim damages from the member. In this case, the member may be
                            exempted from liability unless he or she proves that there is no intention or negligence.
                            None.<br><br>

                            <b>Article 14 (Exemption)</b><br>
                            1. The company is not responsible for any damage caused to the member if the service cannot
                            be provided in any of the following cases.<br>
                            1) In case of natural disaster or force majeure equivalent <br>
                            2) In case of intentional service interference by a third party that has entered into a
                            service alliance contract with the company to provide services
                            3) If there is a problem in using the service due to reasons attributable to the member<br>
                            4) In the case of reasons without intention or negligence of the company other than
                            subparagraphs 1 to 3 <br>
                            2. The company does not guarantee the reliability, accuracy, etc. of the information, data,
                            facts, etc. posted on the service and the service, and is not responsible for any damages to
                            the member caused by this.<br><br>

                            <b>Article 15 (Application Mutatis Mutandis)</b><br>
                            1. These terms and conditions are stipulated and implemented by the laws of the Republic of
                            Korea.<br>
                            2. Matters not stipulated in these Terms and Conditions shall be governed by relevant laws
                            and commercial practices.<br><br>

                            <b>Article 16 (Settlement of Disputes and Others)</b><br>
                            1. In the event that the parties to a dispute related to location information do not reach a
                            consultation or are unable to reach an agreement, the company pays finances to the Korea
                            Communications Commission pursuant to Article 28 of the Act on the Protection and Use of
                            Location Information, etc.
                            You can apply.<br>
                            2. If there is no agreement between the parties involved in a dispute related to location
                            information or cannot be reached, you may apply for mediation to the Personal Information
                            Dispute Mediation Committee pursuant to Article 43 of the Personal Information Protection
                            Act.
                            Yes.<br><br>

                            <b>Article 17 (Company contact information)</b> <br>
                            The company name and address are as follows.<br>
                            1. Trade name: LS Innovation <br>
                            2. Address: #901, Open M Tower, 242 Gonghang-daero, Gangseo-gu, Seoul <br>
                            3. Main phone number: 1600-3532<br>
                            4. Email Address: lsinnovation@naver.com<br><br>

                            <b>Appendix</b><br>
                            Article 1 (Enforcement Date) These terms and conditions are effective from October 1,
                            2019.<br>
                            Article 2 The person in charge of personal location information is designated as follows as
                            of November 16, 2017.<br>
                            Person in charge of personal location information Manager of personal location
                            information<br>
                            - Name: Sang-Hoon Lee (R&D Center affiliated with the company)<br>
                            - Contact: 1600-3532
                        </p>
                    </div>
                </div>
                <!-- //modal body -->
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>



    <!-- modal : 입점신청 -->
    <div id="modalStoreApplication" class="modal fade" tabindex="-1" role="dialog"
        aria-labelledby="modalStoreApplicationLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content pad">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="modalStoreApplicationLabel">Apply for store</h3>
                </div>
                <!-- // modal header -->
                <div class="modal-body">
                    <form method="post" id="addmission_reg" name="addmission_reg" enctype='multipart/form-data'
                        onsubmit="return false;">
                        <div class="base-form store-application">
                            <ul class="form-list long">
                                <li>
                                    <div class="col">
                                        <p class="tit"><span class="red">*</span><span>Business name</span></p>
                                        <div class="area">
                                            <div class="input">
                                                <input type="text" id="modalAdmissionShopName" name="AdmissionShopName"
                                                    class="form-control shop-name" value="" required
                                                    oninput="importRequest(this)">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <p class="tit"><span class="red">*</span><span>Phone number where you can be
                                                reached</span></p>
                                        <div class="area">
                                            <div class="input">
                                                <input type="text" id="modalAdmissionShopNumber"
                                                    name="AdmissionShopNumber" class="form-control call-check"
                                                    maxlength="13" value="" required oninput="importRequest(this)">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <p class="tit"><span class="red">*</span><span>Address</span></p>
                                        <div class="area">
                                            <div class="group-area">
                                                <input type="text" id="modalAdmissionShopPost" name="AdmissionShopPost"
                                                    class="form-control post" readonly>
                                                <button type="button" class="btn btn-gray"
                                                    onclick="postSearch(this)">Postcode</button>
                                            </div>
                                            <div class="group-area half">
                                                <input type="hidden" id="modalAdmissionShopOldAddress"
                                                    name="AdmissionShopOldAddress" value="">
                                                <input type="hidden" id="modalAdmissionShopsido"
                                                    name="AdmissionShopsido" value="">
                                                <input type="hidden" id="modalAdmissionShopgungu"
                                                    name="AdmissionShopgungu" value="">
                                                <input type="hidden" id="modalAdmissionShopdong"
                                                    name="AdmissionShopdong" value="">

                                                <input type="text" id="modalAdmissionShopAddress1"
                                                    name="AdmissionShopAddress1" class="form-control address" readonly
                                                    required>
                                                <input type="text" id="modalAdmissionShopAddress2"
                                                    name="AdmissionShopAddress2" class="form-control address-detail"
                                                    required oninput="importRequest(this)">
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col">
                                        <p class="tit">Content</p>
                                        <div class="area">
                                            <div class="textarea">
                                                <textarea name="modalAdmissionEnquiries" id="modalAdmissionEnquiries"
                                                    rows="3" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="btn-area t-center">
                            <button type="button" class="btn-modal-base" onclick="storeApplication(this)">Apply</button>
                        </div>
                    </form>
                    <!-- // base form-->
                    <div class="customer-center-info">
                        <a href="tel:1600-3532">1600-3532</a>
                        <p class="time">Weekdays 9:00am - 5:30pm</p>
                        <span class="holiday">Closed on weekends and public holidays (lunch break 12pm-1pm)</span>
                    </div>
                </div>
                <!-- // modal body -->
            </div>
        </div>
    </div>
    <!-- //modal : Apply for store entry -->
    <!-- modal : App induce -->
    <!-- <div class="modal fade app-modal" id="modalAppInduction" tabindex="-1" role="dialog"
        aria-labelledby="modalAppInductionLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title blind" id="modalAppInductionLabel">App induction popup</h2>
                    <button type="button" class="modal-close direct" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="app-area">
                        <div class="app-top">
                            <div class="app-icon">
                                <i class="icon-matong" title="Maton"></i>
                            </div>
                            <div class="intro">
                                <span>Use the Matong app</span>
                                <p>Get additional discounts.</p>
                            </div>
                        </div>
                        <button class="app-download" onclick="appDownload()">Download app</button>
                        <div class="btn-area t-center">
                            <a href="javascript:void(0)" class="app-countinue" data-dismiss="modal">Continue to mobile
                                web</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- modal : Direct payment -->
    <div class="modal fade app-base-modal" id="modalNow" tabindex="-1" role="dialog" aria-labelledby="modalNowLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title blind" id="modalNowLabel">App-guided popup</h2>
                    <button type="button" class="modal-close direct" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="app-area">
                        <div class="app-base-top now">
                            <p>Direct payment is only possible in the app.</p>
                            <span>Download the Matong app.</span>
                        </div>
                        <button class="app-download" onclick="appDownload()">Download app</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal : Reservation by phone -->
    <div class="modal fade app-base-modal" id="modalCall" tabindex="-1" role="dialog" aria-labelledby="modalCallLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title blind" id="modalCallLabel">App-guided popup</h2>
                    <button type="button" class="modal-close direct" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="app-area">
                        <div class="app-base-top now">
                            <p>Phone connection is not possible on the web.</p>
                            <span>Call the number below.</span>
                            <span class="num phone050_number">050-1515-1511</span>
                        </div>
                        <button class="app-download no" data-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal : Review Policy -->
    <div id="modalreviewPolicy" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modalreviewPolicyLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="modal-title" id="modalreviewPolicyLabel">Review Policy</h3>
                </div>
                <div class="modal-body">
                    <div class="term-area" id="policy_content1">
                        <p>
                            <b>Write a review</b><br>
                            - Reviews can only be written by logged-in users who have verified mobile phone
                            authentication.<br>
                            - Direct payment &gt; You can write a review after the management time is over.<br>
                            - When making a reservation by phone &gt; After successful call &gt; You can write a review
                            only when talking for more than 30 seconds.<br>
                            - It is impossible to write a review in the same shop within 24 hours. (Only once)<br>
                            - Reviews can be written in 20 or more ~ ​​2000 characters.<br><br>

                            <b>Edit and delete reviews</b><br>
                            - It is not possible to edit a review with a reply from the boss.<br><br>

                            <b>Privacy Review Policy</b><br>
                            - Private reviews are not disclosed to other users, and only the reviewer and the business
                            owner can read the private review.<br>
                            - Matong manages reviews based on the following blind policy to help many customers
                            heal.<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;1) Posts containing profane language <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;2) Posts that repeat simple consonants or list meaningless
                            alphabets, numbers, and special characters without mentioning anything related to massage
                            shops<br>
                            <br>
                            &nbsp;&nbsp;&nbsp;&nbsp;4) Posts judged not to have used the massage shop<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;5) Posts containing personal information<br>
                            &nbsp;&nbsp;&nbsp;&nbsp;6) Posts that are obscene or inappropriate for young people<br>
                            <br><br><br>
                            &nbsp;&nbsp;&nbsp;&nbsp;8) Unfounded slander, false postings<br>
                            <br><br>
                            &nbsp;&nbsp;&nbsp;&nbsp;10) Posts that include photos of managers and bosses in the photos
                            in the review
                            <br><br>

                            <b>Other review policies</b><br>
                            - The entire review may be deleted due to the circumstances of the affiliate shop, such as
                            the change of the owner of the affiliate shop or the change of the business operator.<br>
                            - When you cancel your membership, the registered review is not automatically deleted.
                            Please delete them individually before withdrawal.<br><br>

                            <b>Change of review policy</b><br>
                            - Detailed policy of review is subject to change at any time without notice.<br>
                            - This policy was announced on December 16, 2019.
                        </p>
                    </div>
                </div>
                <div class="modal-footer"></div>
            </div>
        </div>
    </div>

    <!-- modal : Write a review -->
    <div class="modal fade app-base-modal" id="modalReview" tabindex="-1" role="dialog"
        aria-labelledby="modalReviewLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title blind" id="modalReviewLabel">App-guided popup</h2>
                    <button type="button" class="modal-close direct" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!-- <div class="modal-body">
                    <div class="app-area">
                        <div class="app-base-top now">
                            <p>You can write a review only in the app.</p>
                            <span>Download the Matong app.</span>
                        </div>
                        <button class="app-download" onclick="appDownload()">Download app</button>
                    </div>
                </div> -->
            </div>
        </div>
    </div>

    <!-- modal : Get Directions -->
    <div class="modal fade app-base-modal" id="modalRoadSearch" tabindex="-1" role="dialog"
        aria-labelledby="modalRoadSearchLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="modal-title blind" id="modalRoadSearchLabel">App-guided popup</h2>
                    <button type="button" class="modal-close direct" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <input type="hidden" name="mobileName" value="">
                <input type="hidden" name="mobileLAT" value="">
                <input type="hidden" name="mobileLON" value="">
                <div class="modal-body">
                    <div class="app-area">
                        <div class="app-road-area">
                            <p class="tit">Navigation</p>
                            <ul class="app-road-list">
                                <li class="kakao">
                                    <!--target="_blank"-->
                                    <a href="javascript:;" onclick="mobileNavi()">Kakao Navi</a>
                                </li>
                                <li class="map">
                                    <!--target="_blank"-->
                                    <a href="javascript:;" onclick="mobileMap()">Map</a>
                                </li>
                                <li class="address">
                                    <button onclick="roadAddressCopy(event)">Copy address</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>



<script>
    /******************** 앱 유도 팝업 ********************/
    // 윈도우 로드 시$(document).ready(function(){


    $(document).ready(function () {
        $(".owl-carousel").owlCarousel({
            loop: true,
            items: 1,
            autoplay: true,
            autoplayTimeout: 3000
        });
        if (mobilecheck()) {
            // 모바일
            var app_cookie = getCookie('appInduction');
            var ribbon_cookie = getCookie('appRibbon');

            if (app_cookie == '') {
                modalAppInduction();
            } else {
                if (ribbon_cookie == '') {
                    ribbonCreate();
                }
            }
        } else {
            // pc
        }
    });

    // 바로결제
    function modalNow() {
        $('#modalNow').modal('show');
    }
    // 모달 : 리뷰작성
    function modalReview() {
        $('#modalReview').modal('show');
    }

    // 모달 : 리뷰정책
    function modalreviewPolicy() {
        policyreadform(1);
        $('#modalreviewPolicy').modal('show');
    }

    // 모달 : 길찾기
    function modalRoadSearch(shop_name, lon, lat) {
        var shop_name_str = shop_name.replace(/\s/gi, "");
        if (!mobilecheck()) {
            var newWindow = window.open("about:blank");
            newWindow.location.href = 'https://map.kakao.com/link/to/' + shop_name_str + ',' + lat + ',' + lon;
        } else {
            $("input[name='mobileName']").val(shop_name_str);
            $("input[name='mobileLAT']").val(lat);
            $("input[name='mobileLON']").val(lon);
            $('#modalRoadSearch').modal('show');
        }
    }
    Kakao.init('6d72348b2a43fb064062e80c0b0d099f');
    function mobileNavi() {
        var lat = Number($("input[name='mobileLAT']").val());
        var lon = Number($("input[name='mobileLON']").val());
        var name = $("input[name='mobileName']").val();

        //console.log(typeof(lon)+" /// "+typeof(lat));
        //console.log(lon+" /// "+lat);
        Kakao.Navi.start({
            name: name,
            x: lon,
            y: lat,
            coordType: 'wgs84'
        });

        //alert("test");
        /*
        //alert("네비게이션");
        var newWindow = window.open("about:blank");
        newWindow.location.href = '/pages/main/main_shop_detail_map.php?name='+$("input[name='mobileName']").val()+'&lat='+$("input[name='mobileLAT']").val()+'&lon='+$("input[name='mobileLON']").val();
        */
    }

    function mobileMap() {
        var newWindow = window.open("about:blank");
        newWindow.location.href = 'pages/main/main_shop_detail_map49de.html?name=' + $("input[name='mobileName']").val() + '&lat=' + $("input[name='mobileLAT']").val() + '&lon=' + $("input[name='mobileLON']").val();
    }

    // 전화예약
    function modalCall(event, phone) {
        if (!mobilecheck()) {
            event.preventDefault();
            $(".phone050_number").text(phone);
            $('#modalCall').modal('show');
        }
    }

    // 앱 유도 팝업
    function modalAppInduction() {
        $('#modalAppInduction').modal('show');
    }

    // 앱 유도 팝업 꺼질 시
    $(document).on('hide.bs.modal', '#modalAppInduction', function (e) {
        setCookie('appInduction', 'off', 1);

        ribbonCreate();
    });

    // 리본 팝업 생성
    function ribbonCreate() {
        var _body = $('body');
        _body.find('.bottom-ribbon').remove();
        var mark = '<div class="bottom-ribbon">' +
            '<div class="ribbon-logo">' +
            '<i class="icon-matong" title="마통"></i>' +
            '</div>' +
            '<div class="ribbon-text">' +
            '<p><span>마통앱</span>을 이용하시고</p>' +
            '<p><span>추가할인혜택</span> 받으세요.</p>' +
            '</div>' +
            '<div class="btn-area">' +
            '<button class="ribbon-btn" onclick="appDownload()">다운로드</button>' +
            '</div>' +
            '<button class="ribbon-close" onclick="ribbonClose(this)">×</button>' +
            '</div>';

        _body.append(mark);
    }

    // 리본 팝업 닫기
    function ribbonClose(obj) {
        var _this = $(obj);
        _this.closest('.bottom-ribbon').remove();

        setCookie('appRibbon', 'off', 1);
    }

    // 앱 다운로드 링크
    function appDownload() {
        var varUA = navigator.userAgent.toLowerCase(); //userAgent 값 얻기

        if (varUA.indexOf("iphone") > -1 || varUA.indexOf("ipad") > -1 || varUA.indexOf("ipod") > -1) {
            //IOS
            window.location.href = 'https://itunes.apple.com/kr/app/msgtong00/id1179397204?mt=8';
        } else {
            //아이폰  외
            //console.log(1)
            window.location.href = 'https://play.google.com/store/apps/details?id=com.msgtong.matong';
        }
    }

    // 하단 슬라이드
    var main_bottom_dots = $('.main-bottom-dot');
    $('.bottom-visaul-slide').slick({
        arrows: false,
        appendDots: main_bottom_dots,
        dots: true,
        dotsClass: 'custom-dots',
    });

    /******************** 하단 모달 호출 ********************/
    /* FUNCTION : modal - TermsUse */
    // 이용약관 모달 호출
    function modalTermsUse() {
        $('#modalTermsUse').modal('show');
        termreadform(1);
    }

    /* FUNCTION : modal - TermsPersonInfo */
    // 개인정보 취급방침 모달 호출
    function modalTermsPersonInfo() {
        $('#modalTermsPersonInfo').modal('show');
        termreadform(3);
    }

    /* FUNCTION : modal - TermsLocationInfo */
    // 위치정보 이용약관 모달 호출
    function modalTermsLocationInfo() {
        $('#modalTermsLocationInfo').modal('show');
        termreadform(2);
    }

    /* FUNCTION : modal - StoreApplication */
    // 입점신청 모달 호출
    function modalStoreApplication() {
        $('#modalStoreApplication').modal('show');
    }

    /******************** 정책 가져오기 ********************/
    function policyreadform(type) {

        var data = new FormData();
        data.set("policy_type", type);
        data.set("method", "getMainPolicyRead");

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: '/send/main/handler/MainHandler.php',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
        }).done(function (data) {

            var obj = JSON.parse(data);
            $("#policy_content" + type).html(obj.res.POLICY_CONTENT);

        }).always(function (data) {

        });
    }
    function detailMenuToggle(obj) {
        var _this = $(obj);
        var _area = _this.closest('.con-subpage').find('.detail-condition');
        var _body = $('body');

        _area.toggleClass('open');

        // 오버레이 생성
        if (_area.hasClass('open')) {
            _body.find('footer').after('<div class="toggle-overlay fade"></div>');
            $('.toggle-overlay').addClass('show');
        } else {
            $('.toggle-overlay').removeClass('show');
            setTimeout(function() {
                _body.find('.toggle-overlay').remove();
            }, 10)
        }
    }
    /******************** 약관 가져오기 ********************/
    function termreadform(type) {

        var data = new FormData();//form
        data.set("term_type", type);
        data.set("method", "getMainTermRead");

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: '/send/main/handler/MainHandler.php',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
        }).done(function (data) {

            var obj = JSON.parse(data);
            $("#term_content" + type).html(obj.res.TERMS_CONTENT);

        }).always(function (data) {

        });
    }

    /******************** 입점 신청 ********************/
    // 입력 시
    function importRequest(obj) {
        var _this = $(obj);
        var _form = _this.closest('.base-form');
        var _shop_name = _form.find('.shop-name');
        var _num = _form.find('call-check');
        var _post = _form.find('.post');
        var _address = _form.find('.address');
        var _address_detail = _form.find('.address-detail');
        var _btn = _form.next().find('button');

        if (_shop_name.val() !== '' && _num.val() !== '' && _post.val() !== '' && _address.val() !== '' && _address_detail.val() !== '') {
            _btn.addClass('active');
        } else {
            _btn.removeClass('active');
        }
    }
    // FUNCTION : store application */
    // 입점신청 - 신청하기 버튼
    function storeApplication(ths) {
        var _this = $(ths);
        var base = _this.closest('.btn-area').siblings('.base-form');
        var _shop_name = $('#modalAdmissionShopName'); // 업소명
        var shop_name = _shop_name.val();
        var _shop_num = $('#modalAdmissionShopNumber'); // 연락가능한 전화번호
        var shop_num = _shop_num.val();
        var _shop_post = $('#modalAdmissionShopPost'); // 업소 주소 - 우편번호
        var shop_post = _shop_post.val();
        var _shop_address1 = $('#modalAdmissionShopAddress1'); // 업소 주소 - 주소
        var shop_address1 = _shop_address1.val();
        var _shop_address2 = $('#modalAdmissionShopAddress2'); // 업소 주소 - 상세 주소
        var shop_address2 = _shop_address2.val();
        var _shop_enquiry = $('#modalAdmissionEnquiries'); // 문의내용
        var shop_enquiry = _shop_enquiry.val();

        var regExp = /^(01[016789]{1}|02|0[3-9]{1}[0-9]{1})-([0-9]{3,4})-([0-9]{4})$/;

        // 업소명
        if (shop_name == '' || shop_name == undefined) {
            _shop_name.focus();
            _shop_name.addClass('error');
            return false;
        } else {
            _shop_name.removeClass('error');
        }

        // 업소 전화번호
        if (shop_num !== '' && shop_num !== undefined) {
            if (!regExp.test(shop_num)) {
                _shop_num.focus();
                _shop_num.addClass('error');
                _shop_num.siblings('p').remove();
                _shop_num.after('<p class="small orange-d tel-error">올바른 전화번호 형식이 아닙니다.</p>');
                return false;
            } else {
                _shop_num.removeClass('error');
                $(".tel-error").remove();
            }
        } else {
            _shop_num.focus();
            _shop_num.addClass('error');
            return false;
        }

        // 우편번호
        if (shop_post == '' || shop_post == undefined) {
            _shop_post.siblings('button').addClass('error');
            return false;
        } else {
            _shop_post.removeClass('error');
        }

        // 주소
        if (shop_address1 == '' || shop_address1 == undefined) {
            _shop_post.siblings('button').addClass('error');
            return false;
        } else {
            _shop_post.siblings('button').removeClass('error');
        }

        // 주소 - 상세
        if (shop_address2 == '' || shop_address2 == undefined) {
            _shop_address2.focus();
            _shop_address2.addClass('error');
            return false;
        } else {
            _shop_address2.removeClass('error');
        }

        var form = $('#addmission_reg')[0];
        var data = new FormData(form);
        data.set("method", "setAdmissionReg");

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: '/send/main/handler/MainHandler.php',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
        }).done(function (data) {
            //console.log(data);
            var obj = JSON.parse(data);
            if (obj.msg == "fail") {
                return false;
            } else {
                alert('입점 신청이 완료되었습니다.');

                form.reset();
                _this.closest('.modal').modal('hide');
            }
        }).always(function (data) {

        });
    }

    /* FUNCTION : post search */
    // 우편번호 클릭 시
    function postSearch(obj) {
        goAddressPopup();
    }

</script>





<script type="text/javascript">
    nightCheck();
    mainevent();
    latestTurnSet();
    UserStats();

    $(document).ready(function () {
        //history.go(0);
        choicelated();

        $('body').addClass("main");
        if ($("input[name='now_lat']").val() == '') {
            getNowGPSinfo();
        }

        /* GPS 좌표 연동 */
        window.setTimeout(addressinfo, 200);
        window.setTimeout(recommendshopsrcform, 200); // 좌표 불러오고 함수 실행
        $(".area1_name_str").text(main_area);
        //console.log(main_area);
    });
    /******************** 샵 슬라이드 ********************/
    function recommendshopsrcform() {

        var data = new FormData();
        if ($("input[name='now_lat']").val() != '' && $("input[name='now_lat']").val() != undefined) {
            data.append("now_lat", $("input[name='now_lat']").val());
        } else {
            data.append("now_lat", $("input[name='basic_lat']").val());
        }

        if ($("input[name='now_lon']").val() != '' && $("input[name='now_lon']").val() != undefined) {
            data.append("now_lon", $("input[name='now_lon']").val());
        } else {
            data.append("now_lon", $("input[name='basic_lon']").val());
        }

        //return false;
        data.append("area1", $("input[name='area_1_id']").val());

        data.append("method", "getMainRecommendShopList");

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: '/send/main/handler/MainHandler.php',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
        }).done(function (data) {
            var obj = JSON.parse(data);

            var source = $('#entry-RecommendShopList').html();
            var template = Handlebars.compile(source);
            var html = template(obj.res);
            if (obj.res.lists.length == 0) {
                $('.main-shop').hide();
            } else {

                var _slide = $('.main-slide');
                //$('#recommendshoplist').html(html);
                //_slide.html(html);
                _slide.slick('slickRemove', null, null, true);
                _slide.slick('slickAdd', html);
            }
        }).always(function (data) {
            shopImgSlide('.shop-img-list:not(.slick-initialized)');

            setTimeout(function () {
                lazySizes.init();

                $('.loader').fadeOut('slow');
            }, 100);
        });
    }

    /******************** 이벤트 슬라이드 ********************/
    function mainevent() {
        var data = new FormData();
        data.append("method", "getMainEvent");

        $.ajax({
            type: 'POST',
            enctype: 'multipart/form-data',
            url: '/send/main/handler/MainHandler.php',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
        }).done(function (data) {
            var obj = JSON.parse(data);

            var source = $('#entry-EventList').html();
            var template = Handlebars.compile(source);
            var html = template(obj.res);
            $('#mainevent').html(html);

            if (obj.res.nothing != undefined) {
                $(".main-event").hide();
            }
            else {
                // 이벤트 슬라이드
                shopImgSlide('.event-slide');
            }

        }).always(function (data) {
        });
    }

    function andlink() {
        window.location.href = "https://play.google.com/store/apps/details?id=com.msgtong.matong";
    }

    function ioslink() {
        window.location.href = "https://itunes.apple.com/kr/app/apple-store/id1179397204";
    }

    $("#mainLatestInput").on('focus', function (e) {
        $(".main-search").addClass("show");
    }).on('input', function (e) {
        var _ul, _li, i, val = this.value;
        var _main_src = document.querySelector('.main-search');
        var _latest = _main_src.querySelector('.latest-search');
        var _box = _main_src.querySelector('.search-data');
        var _fragment = document.createDocumentFragment();
        _latest.classList.add('hide');

        _box.innerHTML = '';
        if (!val) {
            _latest.classList.remove('hide');
            return false;
        }
        currentFocus = -1;

        var reg = new RegExp('[' + val + ']', 'g');
        //var stat = sortOverlap(val);

        _ul = document.createElement('ul');
        _ul.setAttribute('id', this.id + 'autocomplete-list');
        _ul.setAttribute('class', 'autocomplete-items');

        _box.appendChild(_ul);
        var hangul = gethangul(val);

        if (hangul.length < 2) {
            _li = document.createElement('li');
            _li.classList.add('data-none');
            _li.textContent = '검색 데이터가 존재하지 않습니다.';
            _fragment.appendChild(_li);
        } else {
            var data = new FormData();
            if ($("#mainLatestInput").val() != '' && $("#mainLatestInput").val() != undefined) {
                data.append("srctext", $("#mainLatestInput").val());////hangul
            } else {
                return false;
            }

            data.append("method", "getMainAutoList");

            $.ajax({
                type: 'POST',
                enctype: 'multipart/form-data',
                url: '/send/main/handler/MainHandler.php',
                data: data,
                async: false,
                processData: false,
                contentType: false,
                cache: false,
            }).done(function (data) {
                var obj = JSON.parse(data);
                if (obj.res.subway.length == 0 && obj.res.area.length == 0 && obj.res.shop.length == 0) {
                    _li = document.createElement('li');
                    _li.classList.add('data-none');
                    _li.textContent = '검색 데이터가 존재하지 않습니다.';
                    _fragment.appendChild(_li);
                } else {
                    for (i = 0; i < obj.res.subway.length; i++) {

                        _li = document.createElement('li');
                        _li.setAttribute('class', obj.res.subway[i].TYPE);
                        _li.setAttribute('data-name', obj.res.subway[i].SUBWAY_NAME + '(' + obj.res.subway[i].SUBWAY_NUMBER + ')');
                        _li.setAttribute('data-lat', obj.res.subway[i].SUBWAY_LAT);
                        _li.setAttribute('data-lon', obj.res.subway[i].SUBWAY_LON);
                        _li.setAttribute('data-subway1', obj.res.subway[i].SUBWAY_ID_01);
                        _li.setAttribute('data-subway2', obj.res.subway[i].SUBWAY_ID_02);
                        _li.setAttribute('data-subway3', obj.res.subway[i].SUBWAY_ID_03);
                        _li.setAttribute('onclick', "autosearch(this)");
                        _li.innerHTML = '<a href="javascript:;">' + obj.res.subway[i].SUBWAY_NAME + '(' + obj.res.subway[i].SUBWAY_NUMBER + ')</a>';
                        _fragment.appendChild(_li);

                    }

                    for (i = 0; i < obj.res.area.length; i++) {

                        _li = document.createElement('li');
                        _li.setAttribute('class', obj.res.area[i].TYPE);
                        _li.setAttribute('data-name', obj.res.area[i].AREA_NAME);
                        _li.setAttribute('data-lat', obj.res.area[i].AREA_LAT);
                        _li.setAttribute('data-lon', obj.res.area[i].AREA_LON);
                        _li.setAttribute('onclick', "autosearch(this)");
                        _li.innerHTML = '<a href="javascript:;" >' + obj.res.area[i].AREA_NAME + '</a>';
                        _fragment.appendChild(_li);

                    }

                    for (i = 0; i < obj.res.shop.length; i++) {

                        _li = document.createElement('li');
                        _li.setAttribute('class', obj.res.shop[i].TYPE);
                        _li.setAttribute('data-name', obj.res.shop[i].SHOP_NAME);
                        _li.setAttribute('data-id', obj.res.shop[i].SHOP_ID);
                        _li.setAttribute('data-lat', obj.res.shop[i].SHOP_LAT);
                        _li.setAttribute('data-lon', obj.res.shop[i].SHOP_LON);
                        _li.setAttribute('onclick', "autosearch(this)");
                        _li.innerHTML = '<a href="javascript:;">' + obj.res.shop[i].SHOP_NAME + '</a>';
                        _fragment.appendChild(_li);

                    }
                }
                _ul.appendChild(_fragment);
            }).always(function (data) {

            });
        }
        _ul.appendChild(_fragment);
    }).on('keydown', function (e) {
        var keyCode = e.which || e.keyCode;

        if (prev_src_id != $(this).attr("id")) {
            prev_src_id = $(this).attr("id");
            currentFocus = -1;
        }

        var _latest = this.parentNode.nextElementSibling.querySelector('.latest-search');
        var _latest_list = _latest.querySelector('.latest-list');
        var _auto = this.parentNode.nextElementSibling.querySelector('.search-data');
        var _auto_list = _auto.querySelector('.autocomplete-items');
        var x;

        if (_latest.classList.contains('hide')) {
            x = _auto_list;
        } else {
            x = _latest_list;
            if (currentFocus == undefined) {
                currentFocus = -1;
            }
        }

        if (x) {
            x = x.getElementsByTagName('li');
        }

        var _ipt = document.getElementById('mainLatestInput');

        if (keyCode == 40) {
            currentFocus++;
            addActive(x, _ipt);
            changeVal(x, _ipt, currentFocus);
        }
        else if (keyCode == 38) {
            currentFocus--;
            addActive(x, _ipt, currentFocus);
            changeVal(x, _ipt, currentFocus);
        }
    });
    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
})
</script>



</body>

<!-- Mirrored from www.msgtong.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 09 Aug 2021 16:50:16 GMT -->

</html>