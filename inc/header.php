
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Massage</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.0/css/bootstrap.min.css" integrity="sha512-F7WyTLiiiPqvu2pGumDR15med0MDkUIo5VTVyyfECR5DZmCnDhti9q5VID02ItWjq6fvDfMaBaDl2J3WdL1uxA==" crossorigin="anonymous" referrerpolicy="no-referrer" />arou
     <!-- Link Swiper's CSS -->
     <link
      rel="stylesheet"
      href="https://unpkg.com/swiper/swiper-bundle.min.css"
    />
    <link rel="stylesheet" href="css/ion.rangeSlider.css">
    <link rel="stylesheet" href="css/jquery.scrollbar.css">
    <link rel="stylesheet" href="css/matong.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mystyle.css">
    <link rel="stylesheet" href="css/multirange.css">
    <!-- <link rel="stylesheet" href="css/style2.css"> -->
</head>


<!-- header -->
<header>
    <div class="con-wrap">
        <h1>
            <a href="index.php" style="font-size: 1.5rem;">
                Massage
            </a>
        </h1>
        <button class="header-btn" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"></button>
        <div class="header-search">
            <form method="post" id="public_src" name="public_src" enctype='multipart/form-data' action="https://www.msgtong.com/pages/main/main_src.php" data-src="" onsubmit="return shopsrc();">
                <input type="hidden" name="method" value="getMainSearch">
                <!-- 허용 안했을 시 -->
                <input type="hidden" name="basic_lat" value="37.4979311">
                <input type="hidden" name="basic_lon" value="127.0275846">
                <!--현재 접속한 위치-->
                <input type="hidden" name="now_lat" value="">
                <input type="hidden" name="now_lon" value="">
                <!--지하철 위치-->
                <input type="hidden" name="subway_lat" value="">
                <input type="hidden" name="subway_lon" value="">
                <!--지하철 id-->
                <input type="hidden" name="subway_1_id" value="0">
                <input type="hidden" name="subway_2_id" value="1">
                <input type="hidden" name="subway_3_id" value="0">
                <input type="hidden" name="subwayname" value="">
                <!--지역 위치-->
                <input type="hidden" name="area_lat" value="">
                <input type="hidden" name="area_lon" value="">
                <!--지역 id-->
                <input type="hidden" name="area_1_id" value="1">
                <input type="hidden" name="area_2_id" value="0">
                <input type="hidden" name="areaname" value="">
                <!--shop id-->
                <input type="hidden" name="shop_id" value="">
                <input type="hidden" name="shopname" value="">
                <input type="hidden" name="srctype" value="">

                <div class="search public-search">
                    <div class="write-area"><!---->
                        <input type="text" id="publicLatestInput" class="latest-input enter-control" name="srctext" autocomplete="off" placeholder="Search by area, subway station, business name" oninput="mainSearchSame(this)">
                        <button type="submit" class="btn-search " title="Search"></button>
                    </div>
                    <div class="search-box">
                        <div class="latest-search">
                            <p class="tit">Recent Search History</p>
                            <div class="latest-list-box">
                                <ul class="latest-list">

                                </ul>
                            </div>
                            <div class="latest-bottom">
                                <a href="javascript:;" class="latest-stat-text" onclick="latestTurn(this)">Turn off recent searches</a>
                                <a href="javascript:;" class="all-delete" onclick="latestDeleteAll('4271553102401')">delete all</a>
                            </div>
                        </div>
                        <div class="search-data"></div>
                    </div>
                </div>
            </form>
        </div>
        <nav class="collapse gnb" id="navbarNavAltMarkup">
            <ul class="gnb-list">
                <li>
                    <a href="around.php">around me</a>
                </li>
                <li>
                    <a href="#">reverse search</a>
                </li>
                <li>
                    <a href="#">local search</a>
                </li>
                <!-- <li>
                    <a href="#">map search</a>
                </li> -->
                <li class="customer">
                    <a href="service_choice.php">Service center</a>
                </li>
                <li>
                <a href="login.php">login</a>
                </li>
            </ul>
        </nav>
        <button type="button" class="header-search-btn" onclick="headSearchToggle(this)"></button>
    </div>
</header>
<!-- main visual -->
<div class="main-body">