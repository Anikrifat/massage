<div class="left-menu" id="serviceleftmenu">
    <h2 class="menu-tit"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Service center</font></font></h2>
    <!-- 선택된 메뉴 li class active -->
    <ul class="left-menu-list">
        <li class="active">
            <a href="service_choice.php"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Favorite Shop List</font></font></a>
        </li>
        <li>
            <a href="service_latest.php"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">List of recently viewed shops</font></font></a>
        </li>
        <li>
            <a href="service_event.php"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">event</font></font></a>
        </li>
        <li>
            <a href="service_notice.php"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Notice</font></font></a>
        </li>
        <li>
            <a href="service_ask.php"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Frequently Asked Questions</font></font></a>
        </li>
        <li>
            <a href="service_entering.php"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Store information</font></font></a>
        </li>
    </ul>
    <button class="left-toggle" onclick="leftMenuToggle(this)">
        <span class="blind"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Left menu toggle button</font></font></span>
    </button>
</div>