<?php include('inc/header.php'); ?>
<!-- main visual -->
<section class="main-visual-area">
    <h2 class="blind">full search</h2>
    <!--onsubmit="return false;"onsubmit="return false;"  onsubmit="return false;"-->
    <form method="post" id="main_src" name="main_src" enctype='multipart/form-data' action="https://www.msgtong.com/pages/main/main_src.php" data-src="" onsubmit="return shopsrc();">
        <input type="hidden" name="method" value="getMainSearch">
        <input type="hidden" id="srctype" name="srctype" value="main">
        <!--현재 접속한 위치-->
        <input type="hidden" name="now_lat" value="">
        <input type="hidden" name="now_lon" value="">
        <!--지하철 위치-->
        <input type="hidden" name="subway_lat" value="">
        <input type="hidden" name="subway_lon" value="">
        <!--지하철 id-->
        <input type="hidden" name="subway_1_id" value="">
        <input type="hidden" name="subway_2_id" value="">
        <input type="hidden" name="subway_3_id" value="">
        <input type="hidden" name="subwayname" value="">
        <!--지역 위치-->
        <input type="hidden" name="area_lat" value="">
        <input type="hidden" name="area_lon" value="">
        <!--지역 id-->
        <input type="hidden" name="area_1_id" value="">
        <input type="hidden" name="area_2_id" value="">
        <input type="hidden" name="areaname" value="">
        <!--shop id-->
        <input type="hidden" name="shop_id" value="">
        <input type="hidden" name="shopname" value="">
        <input type="hidden" name="srctype" value="">
        <!-- pagetype -->
        <input type="hidden" name="pgtype" value="main">
        <div class="main-visual">
            <div class="con-wrap">
                <p class="tit">Member's healing time</p>
                <div class="search main-search">
                    <div class="write-area">
                        <!--onclick="shopsrc()"-->
                        <input type="text" id="mainLatestInput" class="latest-input enter-control" name="srctext" value="" autocomplete="off" placeholder="Search by area, subway station, business name" oninput="mainSearchSame(this)">
                        <button type="submit" class="btn-search " title="검색"></button>
                    </div>
                    <div class="search-box">
                        <div class="latest-search">
                            <p class="tit">Recent Search History</p>
                            <div class="latest-list-box">
                                <ul class="latest-list">
                                </ul>
                            </div>
                            <div class="latest-bottom">
                                <a href="# class=" latest-stat-text" onclick="latestTurn()">Turn off recent
                                    searches</a>
                                <a href="# class=" all-delete" onclick="latestDeleteAll('4271553102401')">delete all</a>
                            </div>
                        </div>
                        <div class="search-data"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<!-- area search -->
<section class="area-search-area">
    <div class="area-search">
        <h2 class="area-tit">local search</h2>
        <form method="post" id="area_src" name="area_src" enctype='' action="#">
            <input type="hidden" id="area_city" name="area_1_id" value="">
            <input type="hidden" id="area_zone" name="area_2_id" value="">
        </form>
        <div class="area-box-all">
            <div class="area-box city">
                <button class="area-label" id="areaCity" aria-haspopup="true" aria-expanded="true" onclick="areaLabelClick(this, event)"><span class="area1_name">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Seoul</font>
                        </font>
                    </span></button>
                <!-- <ul id="area_1" class="area-list list-city show" aria-labelledby="areaCity">

                    <li class="active"><a href="#(0)" data-city-id="1" data-city="서울">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Seoul</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="2" data-city="경기">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">game</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="3" data-city="경남">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gyeongnam</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="4" data-city="경북">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gyeongbuk</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="5" data-city="광주">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gwangju</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="6" data-city="대구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Dae-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="7" data-city="대전">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Daejeon</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="8" data-city="부산">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Busan</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="9" data-city="울산">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Ulsan</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="10" data-city="인천">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Incheon</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="11" data-city="전남">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Jeonnam</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="12" data-city="전북">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Jeonbuk</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="13" data-city="제주">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Jeju</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="14" data-city="충남">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Chungnam</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="15" data-city="충북">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Chungbuk</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="16" data-city="강원">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gangwon</font>
                            </font>
                        </a></li>
                </ul> -->
            </div>
            <div class="area-box zone">
                <button class="area-label" id="areaZone" aria-haspopup="true" aria-expanded="true" onclick="areaLabelClick(this, event)"><span class="area2_name">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Seoul recommended shops</font>
                        </font>
                    </span></button>
                <!-- <ul id="area_2" class="area-list list-zone show" aria-labelledby="areaZone">

                    <li class="active"><a href="#(0)" data-city-id="0" data-city="서울 추천샵">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Seoul recommended shops</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="17" data-city="강남구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gangnam-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="18" data-city="강동구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gang Dong-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="19" data-city="강북구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gangbuk-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="20" data-city="강서구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gangseo-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="21" data-city="관악구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gwanak-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="22" data-city="광진구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Gwangjin-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="23" data-city="구로구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Guro-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="24" data-city="금천구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Geumcheon-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="25" data-city="노원구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Nowon-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="26" data-city="도봉구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Dobong-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="27" data-city="동대문구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Dongdaemun-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="28" data-city="동작구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Dongjak-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="29" data-city="마포구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Mapo-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="30" data-city="서대문구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Seodaemun-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="31" data-city="서초구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Seocho-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="32" data-city="성동구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Seongdong-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="33" data-city="성북구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Seongbuk-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="34" data-city="송파구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Songpa-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="35" data-city="양천구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Yangcheon-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="36" data-city="영등포구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Yeongdeungpo-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="37" data-city="용산구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Yongsan-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="38" data-city="은평구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Eunpyeong-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="39" data-city="종로구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Jongno-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="40" data-city="중구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Jung-gu</font>
                            </font>
                        </a></li>
                    <li><a href="#(0)" data-city-id="41" data-city="중랑구">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Jungnang-gu</font>
                            </font>
                        </a></li>
                </ul> -->
            </div>
        </div>
    </div>
</section>
<!-- main -->
<main class="contents">
    <div class="con-wrap">
        <div class="main-area">
            <!-- 추천 샵 -->
            <section class="main-shop ">
                <h2><span class="area1_name_str">Seoul</span> Recommended shop</h2>
                <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper-container mySwiper3 homw-service">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <?php include('inc/service-box.php') ?>
                            </div>
                            <div class="swiper-slide">
                                <?php include('inc/service-box.php') ?>
                            </div>
                            <div class="swiper-slide">
                                <?php include('inc/service-box.php') ?>
                            </div>
                            <div class="swiper-slide">
                                <?php include('inc/service-box.php') ?>
                            </div>
                            <div class="swiper-slide">
                                <?php include('inc/service-box.php') ?>
                            </div>
                            <div class="swiper-slide">
                                <?php include('inc/service-box.php') ?>
                            </div>

                        </div>
                        
                    </div>
               
            </section>
            <section class="main-sort">
                <h2>Find the shop that's right for you</h2>
                <ul class="main-sort-list">
                    <li class="th">
                        <a href="#/arounde94b.html?course=1">
                            <div class="img"></div>
                            <span>Thailand</span>
                        </a>
                    </li>
                    <li class="cn">
                        <a href="#/around9232.html?course=2">
                            <div class="img"></div>
                            <span>china</span>
                        </a>
                    </li>
                    <li class="th">
                        <a href="#/arounde94b.html?course=1">
                            <div class="img"></div>
                            <span>Thailand</span>
                        </a>
                    </li>
                    <li class="cn">
                        <a href="#/around9232.html?course=2">
                            <div class="img"></div>
                            <span>china</span>
                        </a>
                    </li>
                    <li class="th">
                        <a href="#/arounde94b.html?course=1">
                            <div class="img"></div>
                            <span>Thailand</span>
                        </a>
                    </li>

                </ul>
            </section>
            <!-- 이벤트-->
            <section class="main-event">
                <div class="box">
                    <div class="left">
                        <h2>event</h2>
                        <p class="text">Don't miss out on current events in Matong!<br>Participate and receive generous
                            gifts!</p>
                    </div>
                    <div class="right">
                        <div class="event-slide" id="mainevent">

                        </div>
                    </div>
                </div>
            </section>

            <!-- 최근 본 샵 -->
            <aside class="latest-view-shop" id="choicerelated" style="top: 150px;">
                <div class="latest-view-box heart">
                    <h2 class="latest-title">
                        <a href="#/_5_service/service_choice.php">
                            <span>찜한 샵</span>
                            <span class="latest-view-count">0</span>
                        </a>
                    </h2>
                    <!-- 최대 15개 까지 -->
                    <button class="latest-view-toggle" data-toggle="collapse" data-target="#heartShopCollapase" aria-expanded="false" aria-controls="heartShopCollapase">
                        <span class="blind">찜한 샵 토글 버튼</span>
                    </button>
                    <div class="latest-view-list-box choicebox">
                        <ul id="heartShopCollapase" class="latest-view-list choicelist collapse">
                            <!-- 데이터가 없을 경우 --->
                            <!-- <li class="data-none">찜한 샵이<br>없습니다.</li> -->
                            <li class="data-none">찜한 샵이<br>없습니다.</li>
                        </ul>
                    </div>
                </div>
                <div class="latest-view-box">
                    <h2 class="latest-title">
                        <a href="#/_5_service/service_latest.php">
                            <span>최근 본 샵</span>
                            <span class="latest-view-count">1</span>
                        </a>
                    </h2>
                    <!-- 최대 15개 까지 -->
                    <div class="latest-view-list-box latedbox">
                        <ul class="latest-view-list latedlist">
                            <!-- 데이터가 없을 경우 --->
                            <!-- <li class="data-none">최근 본 샵이<br>없습니다.</li> -->
                            <li>
                                <a href="#/main/main_shop_detail.php?num=MTHW7LBA">
                                    <div class="latest-view-img">
                                        <img src="https://cdn.msgtong.com/new_partner/_THUMB_400x280_MTHW7LBA_20200318121229.jpg" alt="카운터">
                                    </div>
                                    <div class="latest-view-info">
                                        <div class="latest-view-info-box">
                                            <p class="tit">북경아로마 (마곡)</p>
                                            <p class="text">서울특별시 강서구 마곡중앙6로 63</p>
                                            <span class="text">70,000원</span>
                                        </div>
                                    </div>
                                </a>
                                <button class="latest-view-close" onclick="latestShopDelete('MTHW7LBA')">×</button>
                            </li>

                        </ul>
                    </div>
                </div>
                <button class="latest-toggle" onclick="latestSideToggleButton(event)">
                    <span class="blind">우측 토글 버튼</span>
                </button>
            </aside>

        </div>
    </div>
</main>

<section class="bottom-visual">
    <h2 class="blind">bottom ad</h2>
    <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper-container mySwiper4 bottom-visaul-slide">
                        <div class="swiper-wrapper">
                        <div class="swiper-slide">
            <div class="bottom-visual-box bg-1">
                <div class="con-wrap">
                    <p class="tit">Download the Matong app and enjoy <span>more benefits!</span></p>
                    <p class="sub">#1 in number of affiliates, Google Play Store, <span>App Store popularity # 1</span>
                    </p>
                    <div class="btn-area">
                        <button type="button" onclick="andlink()">Google Play</button>
                        <button type="button" onclick="ioslink()">iOS download</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
            <div class="bottom-visual-box bg-2">
                <div class="con-wrap">
                    <p class="tit">Securing regular customers, <span>Achieving the best advertising effect by attracting
                            new members!</span></p>
                    <p class="sub">#1 in number of affiliates, Google Play Store, <span>App Store popularity # 1</span>
                    </p>
                    <div class="btn-area">
                        <button type="button" onclick="modalStoreApplication()">Apply for advertisement</button>
                    </div>
                </div>
            </div>
        </div>

                        </div>
                        
                    </div>

    <div class="con-wrap">
        <div class="main-bottom-dot"></div>
    </div>
</section>
<?php include('inc/footer.php'); ?>