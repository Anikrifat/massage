'use strict';

/********** 자동 완성 **********/
// FUNCTION : autocomplete
// 자동완성
function autocomplete(inp, arr, cls) {
	var _div = inp.parentNode.parentNode;
	var currentFocus;
	inp.addEventListener('focus', function (e) {
		var _area = pureClosest(inp, cls);
		_area.classList.add('show');
	});
	
	// 값 입력 시
	inp.addEventListener('input', function (e) {
		var _ul, _li, i, val = this.value;
		var _latest = _div.querySelector('.latest-search');
		var _box = _div.querySelector('.search-data');
		var _fragment = document.createDocumentFragment();
		_latest.classList.add('hide');

		_box.innerHTML = '';
		if (!val) {
			_latest.classList.remove('hide');
			return false;
		}
		currentFocus = -1;

		var reg = new RegExp('[' + val + ']', 'g');
		var stat = sortOverlap(val);

		_ul = document.createElement('ul');
		_ul.setAttribute('id', this.id + 'autocomplete-list');
		_ul.setAttribute('class', 'autocomplete-items');

		_box.appendChild(_ul);

		if (stat == false) {
			_li = document.createElement('li');
			_li.classList.add('data-none');
			_li.textContent = '검색 데이터가 존재하지 않습니다.';
			_fragment.appendChild(_li);
		} else {
			for (i = 0; i < arr.length; i++) {
				var arr_name = arr[i].name;
				if (arr_name.indexOf(val) !== -1) {
					var matchArray;
					var resultString = '';
					var first = 0;
					var last = 0;

					// 각각 일치하는 부분 검색
					while ((matchArray = reg.exec(arr_name)) !== null) {
						last = matchArray.index;
						// 일치하는 모든 문자열 연결
						resultString += arr_name.substring(first, last);

						var m_arr;
						if (matchArray[0] == ' ') {
							m_arr = '&nbsp';
						} else {
							m_arr = matchArray[0];
						}
						
						// 일치하는 부분에 강조 class 추가
						resultString += '<span class="auto-high">' + m_arr + '</span>';
						// RegExp 객체의 lastIndex 속성을 이용해 검색 결과의 마지막 인덱스 접근 가능
						first = reg.lastIndex;
					}
					// 문자열 종료
					resultString += arr_name.substring(first, arr_name.length);

					var line_arr = [];
					_li = document.createElement('li');

					if (arr[i].type == 'station') {
						// 지하철 클래스 생성
						var subway = arr[i].subway;
						var subway_leng = subway.line.length;
						for (var j = 0; j < subway_leng; j++) {
							var subway_line = subway.line[j];
							var _span = document.createElement('span');
							_span.classList.add('line');
							_span.classList.add(subway.area + '-' + subway_line[0]);
							_span.textContent = subway_line[1];
							resultString += _span.outerHTML;
						}
					}
					_li.setAttribute('class', arr[i].type);
					_li.setAttribute('data-name', arr[i].name);
					_li.setAttribute('data-location', arr[i].location);

					// a태그 형태 변경 필요
					_li.innerHTML = '<a href="#">' + resultString + '</a>';
					_fragment.appendChild(_li);
				}
			}
		}
		
		_ul.appendChild(_fragment);
	});

	// 키 입력 시
	inp.addEventListener('keydown', function (e) {
		var keyCode = e.which || e.keyCode;
		
		var _latest = this.parentNode.nextElementSibling.querySelector('.latest-search');
		var _latest_list = _latest.querySelector('.latest-list');

		var _auto = this.parentNode.nextElementSibling.querySelector('.search-data');
		var _auto_list = _auto.querySelector('.autocomplete-items');
		var x;
		if (_latest.classList.contains('hide')) {
			x = _auto_list;
		} else {
			x = _latest_list;
			if (currentFocus == undefined) {
				currentFocus = -1;
			}
		}

		if (x) x = x.getElementsByTagName('li');
		if (keyCode == 40) { // down
			currentFocus++;
			addActive(x);
			changeVal(x, currentFocus);
		} else if (keyCode == 38) { //up
			currentFocus--;
			addActive(x);
			changeVal(x, currentFocus);
		} else if (keyCode == 13) { // enter
			e.preventDefault();
			if (currentFocus > -1) {
				if (x) x[currentFocus].querySelector('a').click();
			}
		} else if (keyCode == 27) { // esc
			var _area = pureClosest(inp, cls);
			inp.blur();
			_area.classList.remove('show');
		}
	});

	// 배열 내부 데이터 확인
	function sortOverlap(txt) {
		var re = false;
		arr.forEach(function(i) {
			var arr_name = i.name;
			if ( arr_name.indexOf(txt) != -1) {
				re = true;
			}
		});
		return re;
	}

	// active 클래스 생성
	function addActive(x) {
		if (!x) return false;
		removeActive(x);
		if (currentFocus >= x.length) currentFocus = 0;
		if (currentFocus < 0) currentFocus = (x.length - 1);
		x[currentFocus].classList.add('active');

		scrollSet(x);

		if (!x[0].parentNode.classList.contains('latest-list')) {
			changeVal(x);
		}
	}

	// scroll 세팅
	function scrollSet(x) {
		var position = x[currentFocus].offsetTop - x[currentFocus].parentNode.clientHeight;
		x[currentFocus].parentNode.parentNode.scrollTop = position + x[currentFocus].offsetHeight;
	}

	function changeVal(x) {
		var _con;
		var new_txt = '';

		if (pureClosest(x[currentFocus], '.latest-search') == null) {
			var qs_a = x[currentFocus].querySelector('a');
			if (qs_a != null) {
				_con = qs_a.childNodes;
				var i = '';
				for(i = 0; i < _con.length; i++) {
					if (_con[i].classList == undefined) {
						new_txt += _con[i].textContent;
					} else {
						if (!_con[i].classList.contains('line')) {
							new_txt += _con[i].textContent;
						}
					}
				}
			}
		} else {
			_con = x[currentFocus].querySelector('.tit');
			new_txt += _con.textContent;
		}
		
		
		setTimeout(function() {
			inp.value = new_txt;
			$(inp).setCursorPosition(inp.value.length);
		});
	}

	// active 클래스 제거
	function removeActive(x) {
		for (var i = 0; i < x.length; i++) {
			x[i].classList.remove('active');
		}
	}
}