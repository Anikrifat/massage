'use strict';

/******************** jquery passive warning ********************/
jQuery.event.special.touchstart = {
	setup: function (_, ns, handle) {
		if (ns.indexOf("noPreventDefault") >= 0) {
			this.addEventListener("touchstart", handle, {
				passive: false
			});
		} else {
			this.addEventListener("touchstart", handle, {
				passive: true
			});
		}
	}
};
jQuery.event.special.touchmove = {
	setup: function (_, ns, handle) {
		if (ns.indexOf("noPreventDefault") >= 0) {
			this.addEventListener("touchmove", handle, {
				passive: false
			});
		} else {
			this.addEventListener("touchmove", handle, {
				passive: true
			});
		}
	}
};

// 모바일 체크
function mobilecheck() {
	var check = false;
	(function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true; })(navigator.userAgent || navigator.vendor || window.opera);
	return check;
}

/******************** resize ********************/
// VAR : size
var size = window.matchMedia('(max-width: 768px)');
var size_tab = window.matchMedia('(max-width: 980px)');

/* FUNCTION : resize  */
// 바로 실행
function resizeFast() {
	var _html = document.querySelector('html');
	_html.classList.remove('pc', 'mobile');
	if (size_tab.matches) {
		_html.classList.add('mobile');
	} else {
		_html.classList.add('pc');

		if ($('#modalAppInduction').hasClass('show')) {
			$('#modalAppInduction').modal('hide');
		}
	}
}

/* EVENT : init - resize */
// 리사이즈
resizeFast();

/* EVENT : resize */
// 리사이즈
var timer = null;
$(window).on('resize', function () {
	clearTimeout(timer);
	// timer = setTimeout(resizeDone, 150);
	timer = setTimeout(resizeFast, 0);

	stickyMainSearch();
	stickyLatestShop();
	areaClickSet();
});

// 스크롤 시
$(window).scroll(function () {
	stickyHeader();
	stickyMainSearch();
	stickyLatestShop();

	if ($(this).scrollTop() > 100) {
		$('.go-top').fadeIn();
	} else {
		$('.go-top').fadeOut();
	}
});

/* FUNCTION : sticky - header */
// 스크롤 시 고정 - 헤더
function stickyHeader() {
	var _header = $('header');
	var _scrollTop = $(window).scrollTop();
	if (_scrollTop > 0) {
		_header.addClass('sticky');
	} else {
		_header.removeClass('sticky');
	}
}
stickyHeader();

/******************** 메인 val 연동 ********************/
function mainSearchSame(obj) {
	var _this = $(obj);
	var this_val = _this.val();
	if ($('body').hasClass('main')) {
		var _target;
		if (_this.is('#mainLatestInput')) {
			_target = $('#publicLatestInput');
		} else {
			_target = $('#mainLatestInput');
		}

		_target.val(this_val);
	}
}

/******************** 상단 이동 ********************/
// FUNCTION : goTop 
// 상단 이동
function goTop() {
	$('html, body').animate({
		scrollTop: 0
	}, 400);
	
	return false;
}

/* FUNCTION : sticky - main search */
// 스크롤 시 고정 - 메인 찾아보기
var beforeMainSearch;
function stickyMainSearch() {
	var _window = $(window);
	var _body = $('body');
	var _main = $('.main-search');
	var sticky;
	var _header = $('header');
	var header_height = _header.outerHeight();
	var _public = $('.public-search');
	
	if (_main.length == 1) {
		sticky = _main.position().top;
		sticky = sticky - header_height;

		if (_body.hasClass('over')) {
			if (_window.scrollTop() < beforeMainSearch) {
				_body.removeClass('over');
				_public.removeClass('show on');
			}
		} else {
			beforeMainSearch = sticky;
			if (_window.scrollTop() > sticky) {
				_body.addClass('over');
			}
		}
	}
}
stickyMainSearch();

/******************** 메뉴 클릭 ********************/
// 메뉴 클릭 시
$(document).on({
	'show.bs.collapse' : function() {
		var _search = $('.header-search');
		var _header_btn = $('.header-btn');

		_header_btn.removeClass('search-open');

		$(document).find('footer').after('<div class="overlay fade"></div>');
		$('.overlay').addClass('show');

		_search.removeClass('show');
	},
	'hide.bs.collapse' : function() {
		var _overlay = $('.overlay');
		_overlay.removeClass('show');
		setTimeout(function(){
			_overlay.remove();
		}, 10);
	},
}, '.gnb');

// 오버레이 클릭 시
$(document).on('click', '.overlay', function() {
	var _header_btn = $('.header-btn');

	if (!_header_btn.hasClass('collapsed')) {
		_header_btn.trigger('click');
	}
});

// header search btn
function headSearchToggle(obj) {
	var _search = $('.header-search');
	var _header_btn = $('.header-btn');
	var _header_aria = _header_btn.attr('aria-expanded');

	if (_header_aria == 'true') {
		_header_btn.trigger('click');
	}

	_search.toggleClass('show');
	_header_btn.toggleClass('search-open');
	setTimeout(function() {
		_search.find('.latest-input').focus();
	});
}

/******************** 지역검색 ********************/
// 지역검색 버튼 세팅
function areaClickSet() {
	var _area_btn = $('.area-search').find('.area-tit');
	var _box = $('.area-box-all');
	var txt;

	if (_box.find('.city').length == 1) {
		txt = 'local search';
	} else {
		txt = 'reverse search';
	}

	if (_area_btn.length >= 1) {
		_area_btn.empty();
		if (size.matches) {
			_box.removeClass('open');
			_box.attr('style', '');

			var _a = '<a href="javascript:void(0)" class="m-area-search" onclick="labelToggle(this)">' + txt + '</a>';

			_area_btn.append(_a);
		} else {
			_area_btn.text(txt);
		}
	}
}
areaClickSet();

// 라벨 토글
function labelToggle(obj) {
	var _area = $('.area-search');
	_area.toggleClass('show');
}

/* FUNCTION : area label click */
// 지역검색 - 라벨 클릭 시
function areaLabelClick(obj, e) {
	e.stopPropagation();
	var _this = $(obj);
	var _all = _this.closest('.area-box-all');
	var _label = _all.find('.area-label');
	var _list = _label.siblings('.area-list');
	var leng = _all.find('.area-label').length;
	for (var i = 0; i < leng; i++) {
		var expanded = _label.eq(i).attr('aria-expanded');

		if (expanded === 'true') {
			_label.eq(i).attr('aria-expanded', 'false');
		} else {
			_label.eq(i).attr('aria-expanded', 'true');
		}
	}
	_list.toggleClass('show');
	setTimeout(function() {
		_all.toggleClass('open');
	});

	if(_list.hasClass('show')) {
		_all.css('height', 500);
		_all.off('transitionend');
	} else {
		_all.css('height', 48);

		_all.on('transitionend', function() {
			_all.removeAttr('style');
		});
	}
}

/******************** 샵 이미지 ********************/
/* FUNCTION : shop img - slide */
// 샵 이미지 기본 슬라이드 
function shopImgSlide(obj) {
	$(obj).slick({
		// lazyLoad: 'ondemand',
		fade: true,
		speed: 1000,
		prevArrow: '<button type="button" class="slick-prev"><i class="icon-chevron-left"></i></button>',
		nextArrow: '<button type="button" class="slick-next"><i class="icon-chevron-right"></i></button>'
	});
}

// 슬라이드 화살표 이벤트 방지
$(document).on('click', '.slick-arrow', function (e) {
	e.preventDefault();
});

// 썸네일 클릭 시
function thumDragClick(thum, main) {
	var dragged = 0;
	var threshold = 2;
	thum.find('.slick-slide:not(.slick-cloned)').on({
		mousedown: function () {
			dragged = 0;
		},
		mousemove: function () {
			dragged++;
		},
		mouseup: function () {
			if (dragged < threshold) {
				var idx = $(this).data('slick-index');
				main.slick('slickGoTo', idx);
			}
			dragged = 0;
		}
	});
}

/******************** 슬라이드 ********************/
/* FUNCTION : base slide - reinit */
// 기본 슬라이드 재호출
function baseSlideReInit(clsName) {
	var _this = $(clsName);
	if (_this.is(':visible')) {
		_this.slick('refresh');
	}
}

/* FUNCTION : base slide - init */
// 기본 슬라이드 실행
function baseSlideInit(obj, count) {
	var _this = $(obj);
	// 샵 썸네일 슬라이더
	_this.slick({
		lazyLoad: 'ondemand',
		slidesToShow: count,
		slidesToScroll: count,
		prevArrow: '<button type="button" class="slick-prev" title="prev"><i class="icon-chevron-left"></i></button>',
		nextArrow: '<button type="button" class="slick-next" title="next"><i class="icon-chevron-right"></i></button>',
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}
		]
	});
	_this.slickLightbox({
		lazy: true,
		src: 'src',
		itemSelector: 'img',
		slick: {
			prevArrow: '<button type="button" class="slick-prev" title="prev"><i class="icon-chevron-left"></i></button>',
			nextArrow: '<button type="button" class="slick-next" title="next"><i class="icon-chevron-right"></i></button>'
		}
	});
}

/******************** 최근 검색 내역 ********************/
/* EVENT : map search hide */
// 맵 토글 열렸을 시 , 다른곳 클릭하면 꺼짐
$(document).click(function (e) {
	var _target = $(e.target);
	var _main_search = $('.main-search');
	var _public_search = $('.public-search');
	var _map_search = $('.map-search');
	var _area_search = $('.area-search');
	var _area_box = $('.area-box-all');

	if (_main_search.hasClass('show') && _target.closest('.main-search').length === 0) {
		_main_search.removeClass('show');
		_main_search.find('.latest-input').blur();
	}

	if (_public_search.hasClass('show') && _target.closest('.public-search').length === 0) {
		_public_search.removeClass('show on');
		_public_search.find('.latest-input').blur();
	}

	if (_map_search.hasClass('show') && _target.closest('.map-search').length === 0) {
		_map_search.removeClass('show on');
		_map_search.find('.latest-input').blur();
	}

	if (_area_search.hasClass('show') && _target.closest('.area-search').length === 0) {
		_area_search.removeClass('show on');
	}

	if (_area_box.hasClass('open') && _target.closest('.area-box-all').length === 0) {
		_area_box.eq(0).find('.area-label').eq(0).trigger('click');
	}
});

/******************** 최근 본 샵 ********************/
/* FUNCTION : sticky - latest shop */
// 최근 본 샵 - 반응형에 따른 형태 변화
function stickyLatestShop() {
	var main = $('main');

	var sticky_size = window.matchMedia("(max-width: 1610px)");

	var _window = $(window);

	var _elm = $('.latest-view-shop');

	if (_elm.length == 1) {
		var _list;
		var sub_bar = 0;
		if (main.hasClass('sub')) {
			if (main.hasClass('shop-detail')) {
				_list = $('.shop-detail-area');

				main.hasClass('sticky') ? sub_bar = 60 : sub_bar = 0;
			} else {
				_list = $('.shop-area');
			}
		} else {
			_list = $('.main-shop');
		}
		var list_pos = _window.scrollTop() + _list.get(0).getBoundingClientRect().top;
		
		var _header = $('header');
		var header_height = _header.outerHeight();

		var _search = $('.top-line.top-search');
		var search_height;
		if (_search.length == 1) {
			if (_search == null) {
				search_height = 0;
			} else {
				search_height = _search.outerHeight();
			}
		} else {
			search_height = '';
		}

		var base_pos = list_pos - sub_bar - (header_height + search_height);
		var base_opos = _window.scrollTop() - base_pos;

		if (_elm.length == 1) {
			if (sticky_size.matches) {
				_elm.css('top', (list_pos + 15) + 'px');

				if (_window.scrollTop() > base_pos) {
					_elm.css('top', list_pos - base_pos + 15 + 'px');
				} else {
					if (main.hasClass('sub')) {
						_elm.css('top', (list_pos - window.pageYOffset) + 'px');
					} else {
						_elm.css('top', (607 - window.pageYOffset) + 'px');
					}
				}
			} else {
				if (main.hasClass('sub')) {
					var top;
					if (main.hasClass('shop-detail')) {
						top = 30;
					} else {
						top = 90;
					}

					_elm.css('top', top + 'px');

					if (_window.scrollTop() > base_pos) {
						_elm.css('top', (top + base_opos + 15) + 'px');
					} else {
						_elm.css('top', top + 'px');
					}
				} else {
					_elm.css('top', 0);
					
					if (_window.scrollTop() > base_pos) {
						_elm.css('top', (base_opos + 15) + 'px');
					} else {
						_elm.css('top', 0);
					}
				}
			}
		}
	}
}

/* FUNCTION : latest view shop - button */
// 최근 본 샵 - 화면 사이즈 작을 때 토글 버튼 클릭 시
function latestSideToggleButton(e) {
	var _target = $(e.target);

	if (_target.hasClass('latest-toggle') || _target.parent().hasClass('latest-toggle')) {
		var _btn = $('.latest-view-shop');
		_btn.toggleClass('show');
	}
}

/* FUNCTION : latest view shop - view */
// 최근 본 샵 - 보기 - scroll 생성여부
function latestViewShop() {
	var _box = $('.latest-view-list-box');
	_box.each(function() {
		var _in_this = $(this);
		var _list = _in_this.find('.latest-view-list');
		var leng = _list.find('li').length;

		// 4개 이상일 경우 scroll class 추가
		if (leng > 4) {
			_in_this.addClass('scroll');
			_list.scrollbar();
		}
	});
}

// EVENT : latest View Over
// 최근 본 샵 - over 시
$(document).on({
	'mouseover': function() {
		var _this = $(this);
		var _box = _this.closest('.latest-view-list-box');
		_box.addClass('show');
	},
	'mouseout': function() {
		var _this = $(this);
		var _box = _this.closest('.latest-view-list-box');
		_box.removeClass('show');
	}
}, '.latest-view-list a');

/******************** 탭 버튼 ********************/
$(document).on('click', '.tab-list li a', function (e) {
	var _this = $(this);
	var _ul = _this.closest('.tab-list');
	var _active = _ul.find('.active');

	e.preventDefault();

	_active.removeClass('active');
	_this.closest('li').addClass('active');
});

/* FUNCTION : copy to clipboard */
// 클립보드 복사
function copyToClipboard(val) {
	var t = document.createElement("textarea");
	t.value = val;

	document.body.appendChild(t);
	t.select();
	t.focus();
	t.setSelectionRange(0, 9999);

	document.execCommand('copy');
	document.body.removeChild(t);
}

/******************** 툴팁 ********************/
// 공통 툴 팁
$('[data-toggle=tooltip][data-html=true]').tooltip({
	trigger : 'hover'
});

/******************** 모달 중첩 ********************/
/* EVENT : modal overlap */
// 모달 중첩
$(document).on({
	'show.bs.modal': function () {
		var zIndex = 1050 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function () {
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	},
	'hidden.bs.modal': function () {
		if ($('.modal:visible').length > 0) {
			setTimeout(function () {
				$(document.body).addClass('modal-open');
			}, 0);
		}
	}
}, '.modal');

/******************** 전화번호 체크 ********************/
/* EVENT : call check */
// 전화 번호 입력 시
var call_event;
$(document).on({
	keydown: function (e) {
		call_event = e;
		keyboardOnlyNum(e);
	},
	input: function (e) {
		// e = call_event;
		var key = e.keyCode || e.which;
		if (key === 8) {
			hyphenRemoveFocus(this);
		} else {
			inputTelNumber(this);
		}
		this.value = this.value.replace(/[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/g, '');
	}
}, '.call-check');

/* EVENT : error delete - input write */
// input 필수값 입력시 에러 제거
$(document).on('keyup change', '.error', function () {
	var _this = $(this);
	var ths_val = _this.val();
	if (ths_val !== '' || ths_val !== undefined) {
		_this.removeClass('error');
	}
});

/******************** 공통 도움 함수 ********************/
// pure Closest
function pureClosest(el, selector) {
	var matchesSelector = el.matches || el.webkitMatchesSelector || el.mozMatchesSelector || el.msMatchesSelector;

	while (el) {
		if (matchesSelector.call(el, selector)) {
			return el;
		} else {
			el = el.parentElement;
		}
	}
	return null;
}

/* FUNCTION : keyboard only num */
// 숫자만 입력 가능
// parameter : 이벤트
function keyboardOnlyNum(evt) {
	evt = event || window.event;
	var key = (evt.which) ? evt.which : evt.keyCode;

	// 백스페이스, 탭 키, 엔터 키, 숫자 키, 우측 숫자 키
	if (key === 8 || key === 9 || key === 13 || (key >= 48 && key <= 57) || (key >= 96 && key <= 105)) {
		return;
	}
	// 컨트롤, 알트 키, 메타 키, 방향 키, home 키, end 키, delete 키
	if (evt.ctrlKey || evt.altKey || evt.metaKey || (key >= 37 && key <= 40) || key === 35 || key === 36 || key === 46) {
		return;
	}

	evt.preventDefault();
}

/* FUNCTION  : hyphen reset */
// 하이픈 제거
// parameter : 오브젝트
function hyphenReset(obj) {
	var rVal = $(obj).val();
	rVal = rVal.replace(/-/g, '');
	return rVal;
}

/* FUNCTION : auto hypen */
//전화번호 자동 hypen
// parameter : 오브젝트
function inputTelNumber(obj) {
	var number = obj.value.replace(/[^0-9]/g, "");
	var tel = "";

	// 서울이라는 변수를 선언
	var seoul = 0;

	// 서울 지역번호(02)가 들어가는 경우 1을 삽입
	if (number.substring(0, 2).indexOf('02') == 0) {
		seoul = 1;
	}

	// 문자열을 자르는 기준에서 서울의 값을 뺄쌤(-)한다.
	// 값이 1일경우 -1이 될것이고, 값이 0일경우 변화가 없다.
	if (number.length < (4 - seoul)) {
		return number;
	} else if (number.length < (7 - seoul)) {
		tel += number.substr(0, (3 - seoul));
		tel += "-";
		tel += number.substr(3 - seoul);
	} else if (number.length < (11 - seoul)) {
		tel += number.substr(0, (3 - seoul));
		tel += "-";
		tel += number.substr((3 - seoul), 3);
		tel += "-";
		tel += number.substr(6 - seoul);
	} else {
		tel += number.substr(0, (3 - seoul));
		tel += "-";
		tel += number.substr((3 - seoul), 4);
		tel += "-";
		tel += number.substr(7 - seoul);
	}
	obj.value = tel;
}

/* FUNCTION  : hyphen remove after focus */
// 하이픈 제거 후 포커스 위치 설정
function hyphenRemoveFocus(obj) {
	var _this = $(obj);
	var tval = obj.value;
	var txtLeng = _this.val().length;
	var cot = obj.selectionStart;
	var crVal = hyphenReset(obj);
	var tRegExp = /-/g;

	_this.val(crVal); // 현재밸류 하이픈 제거

	if (tRegExp.test(tval)) { // 하이픈 있는지 없는 지 체크 후 포커스 위치 확인
		if (txtLeng == 9) {
			if (cot >= 6 && cot <= 10) {
				cot = Number(cot) - 1;
			}
		} else if (txtLeng == 11) {
			if (cot >= 3 && cot <= 6) {
				cot = Number(cot) - 1;
			} else if (cot >= 7 && cot <= 11) {
				cot = Number(cot) - 2;
			}
		} else if (txtLeng == 12) {
			var regExp = /^02/;
			if (regExp.test($(obj).val())) {
				if (cot >= 3 && cot <= 7) {
					cot = Number(cot) - 1;
				} else if (cot >= 8 && cot <= 12) {
					cot = Number(cot) - 2;
				}
			} else {
				if (cot >= 4 && cot <= 7) {
					cot = Number(cot) - 1;
				} else if (cot >= 8 && cot <= 12) {
					cot = Number(cot) - 2;
				}
			}
		} else if (txtLeng == 13) {
			if (cot >= 4 && cot <= 8) {
				cot = Number(cot) - 1;
			} else if (cot >= 9 && cot <= 13) {
				cot = Number(cot) - 2;
			}
		}
	}

	_this.focus().setCursorPosition(cot);
}

/* FUNCTION : comma */
// 콤마
function comma(str, demical) {
	var result;
	str = '' + str;
	if (str.indexOf('.') != -1) {
		var parts = str.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		if (parts[1]) {
			parts[1] = parts[1].substr(0, demical);
		}
		result = parts.join(".");
	} else {
		result = str.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
	return result;
}

/* FUNCTION : comma delete */
// 콤마 제거
function uncomma(str) {
	return str.replace(/,/g, '');
}

// FUNCTION : set Caret - Position
// 커서 위치 설정 - core
function setCaretPosition(ctrl, pos) {
	// Modern browsers
	if (ctrl.setSelectionRange) {
		ctrl.focus();
		ctrl.setSelectionRange(pos, pos);

		// IE8 and below
	} else if (ctrl.createTextRange) {
		var range = ctrl.createTextRange();
		range.collapse(true);
		range.moveEnd('character', pos);
		range.moveStart('character', pos);
		range.select();
	}
}

/* PROTOTYPE : cursor position */
// 커서 위치
// parameter : 문자 길이
$.fn.setCursorPosition = function (pos) {
	this.each(function (index, elem) {
		if (elem.setSelectionRange) {
			elem.setSelectionRange(pos, pos);
		} else if (elem.createTextRange) {
			var range = elem.createTextRange();
			range.collapse(true);
			range.moveEnd('character', pos);
			range.moveStart('character', pos);
			range.select();
		}
	});
	return this;
};

function setCookie(cookieName, value, exdays){
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
	document.cookie = cookieName + "=" + cookieValue;
}

function deleteCookie(cookieName){
	var expireDate = new Date();
	expireDate.setDate(expireDate.getDate() - 1);
	document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
}

function getCookie(cookieName) {
	cookieName = cookieName + '=';
	var cookieData = document.cookie;
	var start = cookieData.indexOf(cookieName);
	var cookieValue = '';
	if(start != -1){
		start += cookieName.length;
		var end = cookieData.indexOf(';', start);
		if(end == -1)end = cookieData.length;
		cookieValue = cookieData.substring(start, end);
	}
	return unescape(cookieValue);
}

/******************** loader ********************/
/* EVENT : loader */
// 로더
$(window).on('load', function () {
	$('.loader').fadeOut('slow');
});