<?php include('inc/header.php'); ?>
<style>
    .content.mt-5.pt-5 {
    margin-top: 8rem;
    margin-bottom: 5rem;
}
</style>
<div class="content mt-5 pt-5">
<main class="login">
        <div class="login-area">
            <div class="login-title">
                
                <span>User Registration</span>
            </div>
            <div id="loginForm" class="login-form">
                <fieldset>
                    <legend>Register</legend>
                    <div class="input max line">
                        <input type="email" id="email" name="email" required >
                        <label for="email">Email</label>
                    </div>
                    <div class="input max line">
                        <input type="text" id="name" name="name" required >
                        <label for="name">Name</label>
                    </div>
                    <div class="input max line">
                        <input type="phone" id="phone" name="phone" required >
                        <label for="phone">Contact</label>
                    </div>
                    <div class="input max line">
                        <input type="password" id="loginPw" name="loginPw" required >
                        <label for="loginPw">PASSWORD</label>
                    </div>
                    <!-- <div class="login-etc">
                        <div class="checks">
                            <input type="checkbox" id="idSave" name="idSave" value="1">
                            <label for="idSave">Keep me signed in</label>
                        </div>
                        <div class="login-inquiry">
                            <button type="button" class="login-inquiry" onclick="modalInfo(7)">Affiliate procedure and price information​</button>
                            <button type="button" class="login-inquiry" onclick="modalInquiry()">Affiliate inquiry</button>
                        </div>
                    </div> -->
                    <div class="login-btn">
                        <button type="submit" class="btn-login" >register</button>
                        <button  type="button" class="btn-cacao" id="channel-chat-button" onclick="window.location.href = 'login.php'">login</button>
                    </div>
                    <div class="login-lost">
                        <!-- <button type="button" class="btn-lost-info" onclick="modalCsInquiry()">Forgot ID or password?</button> -->
                    </div>
                    <!-- <div class="btn-area t-center mt20">
                        <a href="https://play.google.com/store/apps/details?id=com.msgtong.partner" class="btn btn-base right-mar" target="_blank">Google Play</ a>
                        <a href="https://apps.apple.com/kr/app/Matong-President/id1530300848" class="btn btn-base" target="_blank">iOS download</a>
                    </div> -->
                </fieldset>
            </div>
        </div>
    </main>
</div>

<?php include('inc/footer.php'); ?>