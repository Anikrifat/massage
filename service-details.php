<?php include('inc/header.php'); ?>
<style>

</style>
<div class="content mt-5 pt-5">
    <div class="con-wrap con-subpage">
        <!-- 본문 -->
        <section class="">

            <div class="shop-detail-area" id="shopdetail">
                <div class="left shop-detail-img">
                    <button class="like" title="steamed" onclick="heartToggle(this)">
                        <i class="icon-heart-line"></i>
                    </button>
                    <div style="--swiper-navigation-color: #fff; --swiper-pagination-color: #fff" class="swiper-container mySwiper2 main-img">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-1.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-2.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-3.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-4.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-5.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-6.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-7.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-8.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-9.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-10.jpg" />
                            </div>
                        </div>
                        <div class="swiper-button-next"></div>
                        <div class="swiper-button-prev"></div>
                    </div>
                    <div thumbsSlider="" class="swiper-container mySwiper thumb-img mt-2">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-1.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-2.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-3.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-4.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-5.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-6.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-7.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-8.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-9.jpg" />
                            </div>
                            <div class="swiper-slide">
                                <img src="https://swiperjs.com/demos/images/nature-10.jpg" />
                            </div>
                        </div>
                    </div>
                </div>

                <div class="right shop-detail-info">
                    <h2 class="tit">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Chinese traditional massage (everyday)</font>
                        </font>
                    </h2>
                    <ul class="shop-detail-stat">
                        <li class="star">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">0.0 (0)</font>
                            </font>
                        </li>
                        <li class="heart">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">12 steamed</font>
                            </font>
                        </li>
                        <!--117-->
                        <li class="location">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">13.9 km</font>
                            </font>
                        </li>
                        <!--0.1-->
                    </ul>
                    <ul class="shop-detail-location">
                        <li>
                            <p class="tit">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">address</font>
                                </font>
                            </p>
                            <div class="area">
                                <div class="address copy">
                                    <a data-toggle="collapse" href="#shopDetailAddress" role="button" aria-expanded="false" aria-controls="shopDetailAddress">
                                        <span class="shop-address">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">1489, Cheonho-daero, Gangdong-gu, Seoul</font>
                                            </font>
                                        </span>
                                        <p class="collapse text" id="shopDetailAddress">
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">4th floor, 358-4 Sangil-dong</font>
                                            </font>
                                        </p>
                                    </a>
                                    <button class="address-copy" onclick="clipBoardCopy(this)">
                                        <span>
                                            <font style="vertical-align: inherit;">
                                                <font style="vertical-align: inherit;">copy</font>
                                            </font>
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </li>
                        <li>
                            <p class="tit">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">walking time</font>
                                </font>
                            </p>
                            <p class="area">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">20 minutes on foot from Sangil-dong Station Exit 5</font>
                                </font>
                            </p>
                        </li>
                        <li>
                            <p class="tit">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Opening hours</font>
                                </font>
                            </p>
                            <p class="area">
                                <span class="time">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">open 24 hours</font>
                                    </font>
                                </span>

                            </p>
                        </li>
                        <li>
                            <p class="tit">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">phone number</font>
                                </font>
                            </p>
                            <p class="area">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">0507-1858-4570</font>
                                </font>
                            </p>
                        </li>


                    </ul>
                    <div class="download-area hide">
                        <button class="btn btn-gray btn-coupon-download" onclick="modalCoupon()">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">0 won discount coupon</font>
                            </font>
                        </button>
                    </div>
                    <div class="shop-detail-btn">
                        <button class="now" onclick="modalNow()">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">direct payment</font>
                            </font>
                        </button>
                        <button class="loaction">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Get Directions</font>
                            </font>
                        </button>
                        <a href="tel:0507-1858-4570" class="call" onclick="modalCall(event, '0507-1858-4570')">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">phone reservation</font>
                            </font>
                        </a>
                    </div>
                </div>
            </div>

            <!-- 사장님 인사말 -->
            <div class="shop-greeting" onclick="modalShopGreeting()" style="display: none;">
                <div class="shop-g-picture">
                    <img src="/images/shop/shop-ceo.jpg" alt="boss photo">
                </div>
                <div class="shop-g-info">
                    <p class="tit ">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">boss greeting</font>
                        </font>
                    </p>
                    <p class="text SHOP_COMMENT">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Director Kim Ma-tong. </font>
                            <font style="vertical-align: inherit;">- 00 Graduation - 00 Doctoral Completion - All 00 Aesthetic 5 years - All 00 Aesthetic 2 years - Current 00 Aesthetic - All 00 Aesthetic 2 years - Current 00...</font>
                        </font>
                    </p>
                </div>
            </div>
            <section class="dt-tab pt-5 mt-5">
                <div class="tabs">
                    <div class="tab-button-outer">
                        <ul id="tab-button">
                            <li><a class="btn btn-block rounded-0" href="#tab01">Price / Reservation</a></li>
                            <li><a class="btn btn-block rounded-0" href="#tab02">Business information</a></li>
                            <li><a class="btn btn-block rounded-0" href="#tab03">review</a></li>

                        </ul>
                    </div>
                    <div class="shop-detail-tab-area">

                        <div id="tab01" class="tab-contents">
                            <div id="shopdetailpage">
                                <div class="shop-detail-tab-content long">
                                    <div class="menu-detail-preview">

                                        <div class="shop-collapse ">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Events and Announcements</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <ul class="shop-detail-menu-list">
                                                <li>
                                                    <div class="top">
                                                        <p class="tit">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Notice</font>
                                                            </font>
                                                            <!--아로마 캔들이벤트(제목)-->
                                                        </p>
                                                        <p class="text">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">*Member price applied when making a reservation in advance.</font>
                                                            </font>
                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="shop-collapse hide">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Now this time instant discount ★</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <div class="collapse show " id="nowTimeSale">
                                                <ul class="shop-detail-menu-list">
                                                </ul>
                                            </div>
                                        </div>

                                        <!-- // 지금 이시간 즉시할인 //-->
                                        <div class="shop-use-time hide">
                                            <p class="tit">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Day and night hours</font>
                                                </font>
                                            </p>
                                        </div>

                                        <div class="shop-collapse ">
                                            <h3>
                                                <a class="tit" data-toggle="collapse" href="#management20510" role="button" aria-expanded="true" aria-controls="managementThai"><span>
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">sports management</font>
                                                        </font>
                                                    </span></a>
                                            </h3>
                                            <div class="collapse show" id="management20510">
                                                <ul class="shop-detail-menu-list  half">
                                                    <li>
                                                        <div class="top">
                                                            <p class="tit">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">60 minutes </font>
                                                                </font>
                                                            </p>
                                                            <p class="text">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">whole body care</font>
                                                                </font>
                                                            </p>
                                                        </div>
                                                        <div class="menu">
                                                            <div class="menu-box">
                                                                <p class="price">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">60,000 won</font>
                                                                    </font>
                                                                </p>
                                                                <div class="area">
                                                                    <p class="percent">
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">33%</font>
                                                                        </font>
                                                                    </p>
                                                                    <p class="price">
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">40,000 won</font>
                                                                        </font>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="shop-collapse ">
                                            <h3>
                                                <a class="tit" data-toggle="collapse" href="#management20511" role="button" aria-expanded="true" aria-controls="managementThai"><span>
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">Aroma Care</font>
                                                        </font>
                                                    </span></a>
                                            </h3>
                                            <div class="collapse show" id="management20511">
                                                <ul class="shop-detail-menu-list  half">
                                                    <li>
                                                        <div class="top">
                                                            <p class="tit">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">60 minutes </font>
                                                                </font>
                                                            </p>
                                                            <p class="text">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">Full body sports + back aroma</font>
                                                                </font>
                                                            </p>
                                                        </div>
                                                        <div class="menu">
                                                            <div class="menu-box">
                                                                <p class="price">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">90,000 won</font>
                                                                    </font>
                                                                </p>
                                                                <div class="area">
                                                                    <p class="percent">
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">22%</font>
                                                                        </font>
                                                                    </p>
                                                                    <p class="price">
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">70,000 won</font>
                                                                        </font>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>
                                                    <li>
                                                        <div class="top">
                                                            <p class="tit">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">90 minutes </font>
                                                                </font><span class="best">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">suggestion</font>
                                                                    </font>
                                                                </span>
                                                            </p>
                                                            <p class="text">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">Whole body sports + whole body aroma</font>
                                                                </font>
                                                            </p>
                                                        </div>
                                                        <div class="menu">
                                                            <div class="menu-box">
                                                                <p class="price">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">110,000 won</font>
                                                                    </font>
                                                                </p>
                                                                <div class="area">
                                                                    <p class="percent">
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">18%</font>
                                                                        </font>
                                                                    </p>
                                                                    <p class="price">
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">90,000 won</font>
                                                                        </font>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab02" class="tab-contents">
                            <div id="shopdetailpage">
                                <div class="shop-detail-tab-content">
                                    <div class="menu-detail-preview">
                                        <div class="shop-collapse">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Basic Information</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <ul class="shop-detail-menu-list">



                                                <li>
                                                    <div class="top">
                                                        <p class="tit">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">holiday</font>
                                                            </font>
                                                        </p>
                                                        <p class="text">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">24/7</font>
                                                            </font>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="top">
                                                        <p class="tit">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Opening hours</font>
                                                            </font>
                                                        </p>
                                                        <p class="text">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Open from 10:30 a.m. to 5:00 a.m. the next day</font>
                                                            </font>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="top">
                                                        <p class="tit">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">address</font>
                                                            </font>
                                                        </p>
                                                        <div class="text">
                                                            <div class="address copy">
                                                                <a data-toggle="collapse" href="#shopDetailAddressDetail" role="button" aria-expanded="false" aria-controls="shopDetailAddress">
                                                                    <span class="shop-address">
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">97, Beudnaru-ro, Yeongdeungpo-gu, Seoul</font>
                                                                        </font>
                                                                    </span>
                                                                    <p class="collapse text" id="shopDetailAddressDetail">
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">121-130 Dangsan-dong River View</font>
                                                                        </font>
                                                                    </p>
                                                                </a>
                                                                <button class="address-copy" onclick="clipBoardCopy(this)">
                                                                    <span>
                                                                        <font style="vertical-align: inherit;">
                                                                            <font style="vertical-align: inherit;">Copy</font>
                                                                        </font>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                            <div id="shopMap" class="shop-detail-map" tabindex="0" style="position: relative; overflow: hidden; background: url(&quot;https://ssl.pstatic.net/static/maps/mantle/1x/pattern_1.png&quot;) 0px 0px repeat transparent;">
                                                                <div class="map-icon">
                                                                    <a href="/pages/main/main_shop_detail_map.php?name=%EB%B8%94%EB%9E%99%EC%8A%A4%EC%9B%A8%EB%94%94%EC%8B%9C(%EB%8B%B9%EC%82%B0)&amp;lat=37.5269584&amp;lon=126.9083544" target="_blank" class="btn-map-expand">
                                                                        <span class="blind">
                                                                            <font style="vertical-align: inherit;">
                                                                                <font style="vertical-align: inherit;">map zoom</font>
                                                                            </font>
                                                                        </span>
                                                                    </a>
                                                                    <button class="btn-map-reset" onclick="mapReset('37.5269584', '126.9083544')">
                                                                        <span class="blind">
                                                                            <font style="vertical-align: inherit;">
                                                                                <font style="vertical-align: inherit;">Map Initialization</font>
                                                                            </font>
                                                                        </span>
                                                                    </button>
                                                                </div>
                                                                <div style="position: absolute; display: block; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; overflow: visible; width: 100%; height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); z-index: 0; cursor: url(&quot;https://ssl.pstatic.net/static/maps/mantle/1x/openhand.cur&quot;), default;">
                                                                    <div style="position: absolute; display: block; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; overflow: visible; width: 100%; height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); z-index: 0;">
                                                                        <div style="overflow: visible; width: 100%; height: 0px; position: absolute; display: block; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; z-index: 1;">
                                                                            <div style="overflow: visible; width: 100%; height: 0px; position: absolute; display: none; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; z-index: 0; user-select: none;"></div>
                                                                            <div style="overflow: visible; width: 100%; height: 0px; position: absolute; display: block; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; z-index: 1; user-select: none;">
                                                                                <div style="position: absolute; top: 0px; left: 0px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; display: block; width: 0px; height: 0px; overflow: visible; box-sizing: content-box !important;">
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: -158px; left: 501px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111742/50775.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 354px; left: 245px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111741/50777.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 98px; left: 501px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111742/50776.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 98px; left: 245px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111741/50776.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 354px; left: 501px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111742/50777.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: -158px; left: 245px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111741/50775.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: -158px; left: 757px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111743/50775.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 354px; left: -11px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111740/50777.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 98px; left: 757px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111743/50776.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 98px; left: -11px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111740/50776.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 354px; left: 757px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111743/50777.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: -158px; left: -11px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111740/50775.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: -158px; left: 1013px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111744/50775.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 354px; left: -267px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111739/50777.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 98px; left: 1013px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111744/50776.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 98px; left: -267px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111739/50776.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: 354px; left: 1013px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111744/50777.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                    <div draggable="false" unselectable="on" style="position: absolute; top: -158px; left: -267px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; opacity: 1; width: 256px; height: 256px;"><img draggable="false" unselectable="on" alt="" crossorigin="anonymous" width="256" height="256" src="https://nrbe.pstatic.net/styles/basic/1627952672/17/111739/50775.png?mt=bg.ol.sw.ar.lko" style="margin: 0px; padding: 0px; border: 0px solid transparent; display: block; user-select: none; -webkit-user-drag: none; box-sizing: content-box !important; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important; opacity: 1; position: absolute; left: 0px; top: 0px; z-index: 0; width: 256px; height: 256px;"></div>
                                                                                </div>
                                                                            </div>
                                                                            <div style="overflow: visible; width: 100%; height: 0px; position: absolute; display: block; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; z-index: 100;">
                                                                                <div style="overflow: visible; width: 100%; height: 0px; position: absolute; display: block; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; z-index: 101;"></div>
                                                                                <div style="overflow: visible; width: 100%; height: 0px; position: absolute; display: block; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; z-index: 103;"></div>
                                                                                <div style="overflow: visible; width: 100%; height: 0px; position: absolute; display: block; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; z-index: 106;">
                                                                                    <div style="position: absolute; top: 69px; left: 400px; z-index: 0; margin: 0px; padding: 0px; border: 0px solid transparent; display: block; width: 0px; height: 0px; overflow: visible; cursor: default; box-sizing: content-box !important;">
                                                                                        <div style="position: absolute; top: 0px; left: 0px; z-index: 0; margin: 0px; padding: 0px; border: 1px solid transparent; display: block; cursor: default; box-sizing: content-box !important; background: transparent;">
                                                                                            <div style="margin: 0px; padding: 0px; border: 0px solid transparent; display: inline-block; box-sizing: content-box !important; width: 159px; height: 40px;">
                                                                                                <div class="custom-overlay location">
                                                                                                    <p>
                                                                                                        <font style="vertical-align: inherit;">
                                                                                                            <font style="vertical-align: inherit;">Black Swedish (Dangsan)</font>
                                                                                                        </font>
                                                                                                    </p>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div style="margin: 0px; padding: 0px; display: none; width: 0px; height: 0px; position: absolute; border-width: 0px 10px; border-style: solid; border-color: transparent; border-image: initial; pointer-events: none; box-sizing: content-box !important; bottom: -1px; left: 70px;"></div>
                                                                                            <div style="margin: 0px; padding: 0px; display: none; width: 0px; height: 0px; position: absolute; border-width: 0px 10px; border-style: solid; border-color: rgb(255, 255, 255) transparent transparent; border-image: initial; pointer-events: none; box-sizing: content-box !important; bottom: 2px; left: 70px;"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div style="position: absolute; display: none; margin: 0px; padding: 0px; border: 0px none; top: 0px; left: 0px; overflow: visible; width: 100%; height: 100%; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); background-color: rgb(255, 255, 255); z-index: 10000; opacity: 0.5;"></div>
                                                                    </div>
                                                                </div>
                                                                <div style="position: absolute; z-index: 100; margin: 0px; padding: 0px; pointer-events: none; bottom: 0px; right: 0px;">
                                                                    <div style="border: 0px none; margin: 0px; padding: 0px; pointer-events: none; float: right; height: 21px;">
                                                                        <div style="position: relative; width: 53px; height: 14px; margin: 0px 12px 7px 2px; overflow: hidden; pointer-events: auto;"><span style="display:block;margin:0;padding:0 4px;text-align:center;font-size:10px;line-height:11px;font-family:Helvetica,AppleSDGothicNeo-Light,nanumgothic,NanumGothic,&quot;나눔고딕&quot;,Dotum,&quot;돋움&quot;,sans-serif;font-weight:bold;color:#000;text-shadow:-1px 0 rgba(255, 255, 255, 0.3), 0 1px rgba(255, 255, 255, 0.3), 1px 0 rgba(255, 255, 255, 0.3), 0 -1px rgba(255, 255, 255, 0.3);">50m</span><img src="https://ssl.pstatic.net/static/maps/mantle/1x/scale-normal-b.png" width="45" height="4" alt="" style="position: absolute; left: 4px; bottom: 0px; z-index: 2; display: block; width: 45px; height: 4px; overflow: hidden; margin: 0px; padding: 0px; border: 0px none; max-width: none !important; max-height: none !important; min-width: 0px !important; min-height: 0px !important;"><img src="https://ssl.pstatic.net/static/maps/mantle/1x/scale-normal-l.png" width="4" height="10" alt="" style="position:absolute;left:0;bottom:0;z-index:2;display:block;width:4px;height:10px;overflow:hidden;margin:0;padding:0;border:0 none;max-width:none !important;max-height:none !important;min-width:0 !important;min-height:0 !important;"><img src="https://ssl.pstatic.net/static/maps/mantle/1x/scale-normal-r.png" width="4" height="10" alt="" style="position:absolute;right:0;bottom:0;z-index:2;display:block;width:4px;height:10px;overflow:hidden;margin:0;padding:0;border:0 none;max-width:none !important;max-height:none !important;min-width:0 !important;min-height:0 !important;"></div>
                                                                    </div>
                                                                    <div style="border: 0px none; margin: -1px 0px 0px; padding: 0px; pointer-events: none; float: right; height: 22px;"><a href="https://ssl.pstatic.net/static/maps/mantle/notice/legal.html" target="_blank" style="display: block; width: 48px; height: 17px; overflow: hidden; margin: 0px 5px 5px 12px; pointer-events: auto;"><img src="https://ssl.pstatic.net/static/maps/mantle/1x/naver-normal-new.png" width="48" height="17" alt="NAVER" style="display:block;width:48px;height:17px;overflow:hidden;border:0 none;margin:0;padding:0;max-width:none !important;max-height:none !important;min-width:0 !important;min-height:0 !important;"></a></div>
                                                                </div>
                                                                <div style="position: absolute; z-index: 100; margin: 0px; padding: 0px; pointer-events: none; bottom: 0px; left: 0px;">
                                                                    <div style="border: 0px none; margin: 0px; padding: 0px; pointer-events: none; float: left; height: 19px;">
                                                                        <div class="map_copyright" style="margin: 0px; padding: 0px 0px 2px 10px; height: 19px; line-height: 19px; color: rgb(68, 68, 68); font-family: Helvetica, AppleSDGothicNeo-Light, nanumgothic, NanumGothic, 나눔고딕, Dotum, 돋움, sans-serif; font-size: 11px; clear: both; white-space: nowrap; pointer-events: none;">
                                                                            <div style="float: left;"><span style="white-space: pre; color: rgb(68, 68, 68);">© NAVER Corp.</span></div><a href="#" style="font-family: Helvetica, AppleSDGothicNeo-Light, nanumgothic, NanumGothic, 나눔고딕, Dotum, 돋움, sans-serif; font-size: 11px; line-height: 19px; margin: 0px 0px 0px 5px; padding: 0px; color: rgb(68, 68, 68); float: left; pointer-events: auto; text-decoration: underline; display: none;">더보기</a>
                                                                            <div style="float: left;"><a target="_blank" href="http://www.openstreetmap.org/copyright" style="pointer-events: auto; white-space: pre; display: none; color: rgb(68, 68, 68);"> /OpenStreetMap</a></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style="border: 1px solid rgb(41, 41, 48); background: rgb(255, 255, 255); padding: 15px; color: rgb(51, 51, 51); position: absolute; font-size: 11px; line-height: 1.5; clear: both; display: none; max-width: 350px !important; max-height: 300px !important;">
                                                                    <h5 style="font-size: 12px; margin-top: 0px; margin-bottom: 10px;">지도 데이터</h5><a href="#" style="position: absolute; top: 8px; right: 8px; width: 14px; height: 14px; font-size: 14px; line-height: 14px; display: block; overflow: hidden; color: rgb(68, 68, 68); text-decoration: none; font-weight: bold; text-align: center;">x</a>
                                                                    <div><span style="white-space: pre; color: rgb(68, 68, 68); float: left;">© NAVER Corp.</span><a target="_blank" href="http://www.openstreetmap.org/copyright" style="pointer-events: auto; white-space: pre; color: rgb(68, 68, 68); float: left; display: none;"> /OpenStreetMap</a></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="top">
                                                        <p class="tit">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">subway</font>
                                                            </font>
                                                        </p>
                                                        <p class="text">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;"> Seoul Metropolitan Area / Line 5 / Yeongdeungpo Market Station Exit 2 10 minutes on foot </font>
                                                            </font>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="top">
                                                        <p class="tit">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">parking</font>
                                                            </font>
                                                        </p>
                                                        <p class="text">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Parking available near the building (inquire at time of reservation)&ZeroWidthSpace;</font>
                                                            </font>
                                                        </p>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="top">
                                                        <p class="tit">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Sleep</font>
                                                            </font>
                                                        </p>
                                                        <p class="text">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">unable to sleep</font>
                                                            </font>
                                                        </p>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- // 기본 정보 -->

                                        <div class="shop-collapse hide">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Thailand available courses</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <ul class="possible-course">
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">tie management</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Aroma management</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">foot care</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Swedish Management</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">couple management</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">spa management</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">other management</font>
                                                    </font>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- // 태국 가능한 코스 -->
                                        <div class="shop-collapse hide">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Chinese courses available</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <ul class="possible-course">
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">sports management</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Aroma management</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">foot care</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">couple management</font>
                                                    </font>
                                                </li>
                                                <li class="">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">other management</font>
                                                    </font>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- // 중국 가능한 코스 -->
                                        <div class="shop-collapse show">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Courses available in Korea</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <ul class="possible-course">
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">dry management</font>
                                                    </font>
                                                </li>
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Aroma management</font>
                                                    </font>
                                                </li>
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">foot care</font>
                                                    </font>
                                                </li>
                                                <li class="show">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Swedish Management</font>
                                                    </font>
                                                </li>
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">couple management</font>
                                                    </font>
                                                </li>
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">acupressure management</font>
                                                    </font>
                                                </li>
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">meridian management</font>
                                                    </font>
                                                </li>
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">correction management</font>
                                                    </font>
                                                </li>
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">spa management</font>
                                                    </font>
                                                </li>
                                                <li class="hide">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">other management</font>
                                                    </font>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- // 한국 가능한 코스 -->
                                        <div class="shop-collapse hide">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Aesthetic course available</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <ul class="possible-course-type">
                                                <li class="hide">
                                                    <p class="tit">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">face management</font>
                                                        </font>
                                                    </p>
                                                    <ul class="possible-course">
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Trouble management (acne / marks / scars)</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Whitening care (melasma, freckles, blemishes)</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Elasticity management (wrinkle, aging)</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">pore management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Play management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">moisture management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">keratin care</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Detox management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">lifting management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">small face care</font>
                                                            </font>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="hide">
                                                    <p class="tit">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">body care</font>
                                                        </font>
                                                    </p>
                                                    <ul class="possible-course">
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">body care</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">body shape management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Detox management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">flexible management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">slimming management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">scrub management</font>
                                                            </font>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="hide">
                                                    <p class="tit">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">other management</font>
                                                        </font>
                                                    </p>
                                                    <ul class="possible-course">
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">wedding management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">prenatal care</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">postpartum care</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">couple management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Management for women only</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">other management</font>
                                                            </font>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- // 에스테틱 가능한 코스 -->
                                        <div class="shop-collapse hide">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">waxing course</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <ul class="possible-course-type">
                                                <li class="hide">
                                                    <p class="tit">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">type of wax</font>
                                                        </font>
                                                    </p>
                                                    <ul class="possible-course">
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Soft/Hard Wax</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Sugaring Wax</font>
                                                            </font>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="hide">
                                                    <p class="tit">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">waxing care</font>
                                                        </font>
                                                    </p>
                                                    <ul class="possible-course">
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Brazillian waxing</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">face waxing</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">body waxing</font>
                                                            </font>
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li class="hide">
                                                    <p class="tit">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">other management</font>
                                                        </font>
                                                    </p>
                                                    <ul class="possible-course">
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">treatment management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">couple management</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">Management for women only</font>
                                                            </font>
                                                        </li>
                                                        <li class="">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">other management</font>
                                                            </font>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- // 왁싱가능한 코스 -->

                                        <div class="shop-collapse">
                                            <h3>
                                                <p class="tit">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">service available</font>
                                                    </font>
                                                </p>
                                            </h3>
                                            <ul class="possible-service info">
                                                <li class="icon free-parking">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">free parking</font>
                                                    </font>
                                                </li>
                                                <li class="icon room-personal">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">private room</font>
                                                    </font>
                                                </li>
                                                <li class="icon shower">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">shower available</font>
                                                    </font>
                                                </li>
                                                <li class="icon calendar">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">Reservation required</font>
                                                    </font>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab03" class="tab-contents">
                            <div id="shopdetailpage">
                                <div class="shop-detail-tab-content small">
                                    <div class="review-area">
                                        <div class="review-top">
                                            <div class="review-score">
                                                <p class="score-now">
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">4.8 </font>
                                                    </font><span>
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">/ 5.0</font>
                                                        </font>
                                                    </span>
                                                </p>
                                                <span>
                                                    <font style="vertical-align: inherit;">
                                                        <font style="vertical-align: inherit;">(109)</font>
                                                    </font>
                                                </span>
                                            </div>
                                            <button class="review-write" onclick="modalReview()">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Write a review</font>
                                                </font>
                                            </button>
                                        </div>
                                        <div class="review-detail">

                                            <div class="review-detail-top">
                                                <div class="checks">
                                                    <input type="checkbox" id="reviewPicture" name="reviewPicture" value="1" onchange="picchange()">
                                                    <label for="reviewPicture">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">View photo reviews only</font>
                                                        </font>
                                                    </label>
                                                </div>
                                                <div class="right">
                                                    <button onclick="modalreviewPolicy()">
                                                        <font style="vertical-align: inherit;">
                                                            <font style="vertical-align: inherit;">Review Policy</font>
                                                        </font>
                                                    </button>
                                                    <div class="dropdown">
                                                        <button class="dropdown-toggle" type="button" id="reviewSortButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <font style="vertical-align: inherit;">
                                                                <font style="vertical-align: inherit;">latest order</font>
                                                            </font>
                                                        </button>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="reviewSortButton">
                                                            <button class="dropdown-item order1 active" type="button" onclick="reviewtaborder(1)">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">latest order</font>
                                                                </font>
                                                            </button>
                                                            <button class="dropdown-item order2" type="button" onclick="reviewtaborder(2)">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">high star rating</font>
                                                                </font>
                                                            </button>
                                                            <button class="dropdown-item order3" type="button" onclick="reviewtaborder(3)">
                                                                <font style="vertical-align: inherit;">
                                                                    <font style="vertical-align: inherit;">low star rating</font>
                                                                </font>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- 5개 까지 이후 페이징 -->
                                            <div class="review-detail-list-area">
                                                <ul class="review-detail-list">
                                                    <li>
                                                        <div class="review-d">
                                                            <div class="review-d-name">
                                                                <p class="name">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">rich man</font>
                                                                    </font>
                                                                </p>
                                                                <!-- 올해 이전 년도는 앞에 년도 붙임-->
                                                                <span class="time">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">06-06 04:27</font>
                                                                    </font>
                                                                </span>
                                                            </div>
                                                            <!-- 점수에 따라 li 개수 -->
                                                            <ul class="star-list">
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                            </ul>
                                                            <div class="review-d-bottom">
                                                                <p class="review-d-menu"></p>
                                                                <p class="review-d-text">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">It was great! </font>
                                                                        <font style="vertical-align: inherit;">Saetbyeol-sam is kind and accepts me even if I say bullshit. I couldn't see her face because of the mask, but the aura of her beauty came out through the mask.</font>
                                                                    </font>
                                                                </p>
                                                            </div>
                                                            <div class="review-d-slide slick-initialized slick-slider">
                                                                <div aria-live="polite" class="slick-list draggable">
                                                                    <div class="slick-track" style="opacity: 1; width: 0px; transform: translate3d(0px, 0px, 0px);" role="listbox"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>
                                                    <li>
                                                        <div class="review-d">
                                                            <div class="review-d-name">
                                                                <p class="name">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">bill23</font>
                                                                    </font>
                                                                </p>
                                                                <!-- 올해 이전 년도는 앞에 년도 붙임-->
                                                                <span class="time">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">05-26 23:36</font>
                                                                    </font>
                                                                </span>
                                                            </div>
                                                            <!-- 점수에 따라 li 개수 -->
                                                            <ul class="star-list">
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                            </ul>
                                                            <div class="review-d-bottom">
                                                                <p class="review-d-menu"></p>
                                                                <p class="review-d-text">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">I was healed happily in a comfortable atmosphere..hehe I heard the name.. ㅠㅠ I couldn't even make eye contact... I forgot... The manager's tattoo is so pretty and she's a great beauty...!!! </font>
                                                                        <font style="vertical-align: inherit;">Above all, the management was very good ^^. I will come again ^^</font>
                                                                    </font>
                                                                </p>
                                                            </div>
                                                            <div class="review-d-slide slick-initialized slick-slider">
                                                                <div aria-live="polite" class="slick-list draggable">
                                                                    <div class="slick-track" style="opacity: 1; width: 0px; transform: translate3d(0px, 0px, 0px);" role="listbox"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>
                                                    <li>
                                                        <div class="review-d">
                                                            <div class="review-d-name">
                                                                <p class="name">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">double storey man</font>
                                                                    </font>
                                                                </p>
                                                                <!-- 올해 이전 년도는 앞에 년도 붙임-->
                                                                <span class="time">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">05-25 03:09</font>
                                                                    </font>
                                                                </span>
                                                            </div>
                                                            <!-- 점수에 따라 li 개수 -->
                                                            <ul class="star-list">
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                            </ul>
                                                            <div class="review-d-bottom">
                                                                <p class="review-d-menu"></p>
                                                                <p class="review-d-text">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">Here, thanks to Saetbyul, I am completely healed! </font>
                                                                        <font style="vertical-align: inherit;">I liked it so much that I just watched it, so I joined as a member of Matong and commented for the first time. </font>
                                                                        <font style="vertical-align: inherit;">I fell in love with her beauty and skills. Currently my favorite one in my heart😍</font>
                                                                    </font>
                                                                </p>
                                                            </div>
                                                            <div class="review-d-slide slick-initialized slick-slider">
                                                                <div aria-live="polite" class="slick-list draggable">
                                                                    <div class="slick-track" style="opacity: 1; width: 0px; transform: translate3d(0px, 0px, 0px);" role="listbox"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>
                                                    <li>
                                                        <div class="review-d">
                                                            <div class="review-d-name">
                                                                <p class="name">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">dawn moonlight</font>
                                                                    </font>
                                                                </p>
                                                                <!-- 올해 이전 년도는 앞에 년도 붙임-->
                                                                <span class="time">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">05-21 14:38</font>
                                                                    </font>
                                                                </span>
                                                            </div>
                                                            <!-- 점수에 따라 li 개수 -->
                                                            <ul class="star-list">
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                            </ul>
                                                            <div class="review-d-bottom">
                                                                <p class="review-d-menu"></p>
                                                                <p class="review-d-text">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">It's been a long time since I've had time during the day, so I've seen Sia-sensei. </font>
                                                                        <font style="vertical-align: inherit;">I feel like I want to come every day, sir, I'm having a great time.</font>
                                                                    </font>
                                                                </p>
                                                            </div>
                                                            <div class="review-d-slide slick-initialized slick-slider">
                                                                <div aria-live="polite" class="slick-list draggable">
                                                                    <div class="slick-track" style="opacity: 1; width: 0px; transform: translate3d(0px, 0px, 0px);" role="listbox"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>
                                                    <li>
                                                        <div class="review-d">
                                                            <div class="review-d-name">
                                                                <p class="name">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">Bowang</font>
                                                                    </font>
                                                                </p>
                                                                <!-- 올해 이전 년도는 앞에 년도 붙임-->
                                                                <span class="time">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">05-18 21:35</font>
                                                                    </font>
                                                                </span>
                                                            </div>
                                                            <!-- 점수에 따라 li 개수 -->
                                                            <ul class="star-list">
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                                <li class="on"></li>
                                                            </ul>
                                                            <div class="review-d-bottom">
                                                                <p class="review-d-menu"></p>
                                                                <p class="review-d-text">
                                                                    <font style="vertical-align: inherit;">
                                                                        <font style="vertical-align: inherit;">Saetbyul, who is pretty and has the best massages~~ I got a massage without noticing the time passing.</font>
                                                                    </font>
                                                                </p>
                                                            </div>
                                                            <div class="review-d-slide slick-initialized slick-slider">
                                                                <div aria-live="polite" class="slick-list draggable">
                                                                    <div class="slick-track" style="opacity: 1; width: 0px; transform: translate3d(0px, 0px, 0px);" role="listbox"></div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </li>

                                                </ul>
                                            </div>
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- 상세 디테일 탭 -->

            <!-- 이벤트 없는 업소의 경우 이벤트 탭 삭제 -->
            <!-- <ul class="tab-list shop-detail-tab">
                    <li class="nav-link" href="#tab04">
                        <a href="#tab01">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Price / Reservation</font>
                            </font>
                        </a>
                    </li>
                    <li>
                        <a href="#tab02">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Business information</font>
                            </font>
                        </a>
                    </li>

                    <li>
                        <a href="#tab03">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">review</font>
                            </font>
                        </a>
                    </li>
                </ul> -->

            <!-- 페이지 ajax 로드 -->


            <!-- </div> -->

            <!-- 찜한 샵, 최근 본 샵 -->
            <aside class="latest-view-shop" id="choicerelated" style="top: 103px;">
                <div class="latest-view-box heart">
                    <h2 class="latest-title">
                        <a href="/pages/_5_service/service_choice.php">
                            <span>
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Favorite Shop </font>
                                </font>
                            </span>
                            <span class="latest-view-count">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">0</font>
                                </font>
                            </span>
                        </a>
                    </h2>
                    <!-- 최대 15개 까지 -->
                    <button class="latest-view-toggle" data-toggle="collapse" data-target="#heartShopCollapase" aria-expanded="false" aria-controls="heartShopCollapase">
                        <span class="blind">
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Favorite Shop toggle button</font>
                            </font>
                        </span>
                    </button>
                    <div class="latest-view-list-box choicebox">
                        <ul id="heartShopCollapase" class="latest-view-list choicelist collapse">
                            <!-- 데이터가 없을 경우 --->
                            <!-- <li class="data-none">찜한 샵이<br>없습니다.</li> -->
                            <li class="data-none">
                                <font style="vertical-align: inherit;"></font><br>
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">There </font>
                                    <font style="vertical-align: inherit;">are </font>
                                    <font style="vertical-align: inherit;">no </font>
                                    <font style="vertical-align: inherit;">favorite shops </font>
                                    <font style="vertical-align: inherit;">.</font>
                                </font>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="latest-view-box">
                    <h2 class="latest-title">
                        <a href="/pages/_5_service/service_latest.php">
                            <span>
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">Recently viewed shop </font>
                                </font>
                            </span>
                            <span class="latest-view-count">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">2</font>
                                </font>
                            </span>
                        </a>
                    </h2>
                    <!-- 최대 15개 까지 -->
                    <div class="latest-view-list-box latedbox">
                        <ul class="latest-view-list latedlist">
                            <!-- 데이터가 없을 경우 --->
                            <!-- <li class="data-none">최근 본 샵이<br>없습니다.</li> -->
                            <li>
                                <a href="/pages/main/main_shop_detail.php?num=MTEK7PBE">
                                    <div class="latest-view-img">
                                        <img src="https://cdn.msgtong.com/new_partner/_THUMB_400x280_MTEK7PBE_20210702120732.jpg" alt="counter">
                                    </div>
                                    <div class="latest-view-info">
                                        <div class="latest-view-info-box">
                                            <p class="tit">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Chinese traditional massage (everyday)</font>
                                                </font>
                                            </p>
                                            <p class="text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">1489, Cheonho-daero, Gangdong-gu, Seoul</font>
                                                </font>
                                            </p>
                                            <span class="text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">40,000 won</font>
                                                </font>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                                <button class="latest-view-close" onclick="latestShopDelete('MTEK7PBE')">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">×</font>
                                    </font>
                                </button>
                            </li>
                            <li>
                                <a href="/pages/main/main_shop_detail.php?num=MTHW7LBA">
                                    <div class="latest-view-img">
                                        <img src="https://cdn.msgtong.com/new_partner/_THUMB_400x280_MTHW7LBA_20200318121229.jpg" alt="counter">
                                    </div>
                                    <div class="latest-view-info">
                                        <div class="latest-view-info-box">
                                            <p class="tit">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">Beijing Aroma (Magok)</font>
                                                </font>
                                            </p>
                                            <p class="text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">63, Magokjungang 6-ro, Gangseo-gu, Seoul</font>
                                                </font>
                                            </p>
                                            <span class="text">
                                                <font style="vertical-align: inherit;">
                                                    <font style="vertical-align: inherit;">70,000 won</font>
                                                </font>
                                            </span>
                                        </div>
                                    </div>
                                </a>
                                <button class="latest-view-close" onclick="latestShopDelete('MTHW7LBA')">
                                    <font style="vertical-align: inherit;">
                                        <font style="vertical-align: inherit;">×</font>
                                    </font>
                                </button>
                            </li>

                        </ul>
                    </div>
                </div>
                <button class="latest-toggle" onclick="latestSideToggleButton(event)">
                    <span class="blind">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">right toggle button</font>
                        </font>
                    </span>
                </button>
            </aside>

        </section>
    </div>
</div>

<?php include('inc/footer.php'); ?>