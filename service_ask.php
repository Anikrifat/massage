<?php include('inc/header.php'); ?>
<style>

</style>
<div class="content mt-5 pt-5 ">
    <div class="con-wrap con-subpage">
        <?php include('inc/left-menu.php'); ?>
        <!-- 본문 -->
        <section class="sub-page">
            <h3 class="sub-page-tit">
                <span class="subject"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Frequently Asked Questions</font></font></span>
            </h3>

            <!-- <ul class="faq-list">
                <li class="all active">
                    <a href="javascript:;" onclick="asksrcform('src', 'all')"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">all</font></font></a>
                </li>
                <li class="type1">
                    <a href="javascript:;" onclick="asksrcform('src',1)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">use of the site</font></font></a>
                </li>
                <li class="type2">
                    <a href="javascript:;" onclick="asksrcform('src',2)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">event</font></font></a>
                </li>
                <li class="type4">
                    <a href="javascript:;" onclick="asksrcform('src',4)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">review</font></font></a>
                </li>
                <li class="type8">
                    <a href="javascript:;" onclick="asksrcform('src',8)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Coupons / Points</font></font></a>
                </li>
                <li class="type16">
                    <a href="javascript:;" onclick="asksrcform('src',16)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Payment / Receipt</font></font></a>
                </li>
                <li class="type32">
                    <a href="javascript:;" onclick="asksrcform('src',32)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">cancel reservation</font></font></a>
                </li>
                <li class="type64">
                    <a href="javascript:;" onclick="asksrcform('src',64)"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Profile</font></font></a>
                </li>
            </ul> -->

            <form method="post" id="ask_src" name="ask_src" enctype="multipart/form-data" onsubmit="return false;">
                <input type="hidden" name="page" id="page" value="1">
                <input type="hidden" name="type" id="type" value="all">
            </form>

            <div class="tab-content" id="asklist">
    <ul class="accordion-list" id="accordionAsk">
        <li>
            <div class="info">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#askFixed22" role="button" aria-expanded="false" aria-controls="askFixed22" class="collapsed">
                    <div class="tit-area">
                        <p class="tit"><span class="type"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[Use of the site] </font></font></span> <span class="normal"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">How do I agree to receive marketing?</font></font></span></p>
                    </div>
                </a>
            </div>
            <div class="detail collapse" id="askFixed22" data-parent="#accordionAsk" style="">
                <div class="box">
                    <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Go to More at the bottom of the app &gt; My Page &gt; Settings, and turn on 'Agree to receive marketing'.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">In the future, there will be discount coupon events related to receiving marketing consent, so please participate.&nbsp;</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">thank you.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Matong Manager -</font></font></p>
                </div>
            </div>
        </li>
        <li>
            <div class="info">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#askFixed20" role="button" aria-expanded="false" aria-controls="askFixed20">
                    <div class="tit-area">
                        <p class="tit"><span class="type"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[Event] </font></font></span> <span class="normal"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">How to participate in the free coupon event?</font></font></span></p>
                    </div>
                </a>
            </div>
            <div class="detail collapse" id="askFixed20" data-parent="#accordionAsk">
                <div class="box">
                    <p><span style="font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Visit the shop running the free coupon event and receive management during the event period.&nbsp;</font></font></span></p><p><span style="font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">After logging in, you must leave a review to automatically participate in the event.</font></font></span></p><p><span style="font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">※ In order&nbsp; </font></font></span><span style="background-color: rgb(252, 252, 252); color: rgb(51, 51, 51); font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">to participate in the free coupon event, you must pay directly or make a reservation with 050 number and then manage it.</font></font></span></p><p>&nbsp;</p><p><span style="font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Free Coupons We will select the best reviews from the reviews left during the event period and give you free coupons that can be used in the shop.</font></font></span></p><p><span style="font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">(Winners, please check the coupons in the coupon box on My Page.)</font></font></span></p><p>&nbsp;</p><p class="a" style="box-sizing: border-box; border: 0px; text-size-adjust: none; color: rgb(51, 51, 51); font-family: &quot;Noto Sans KR&quot;, &quot;Apple SD Gothic Neo&quot;, &quot;Malgun Gothic&quot;, &quot;맑은 고딕&quot;, dotum, 돋움, sans-serif; font-size: 14px; background-color: rgb(252, 252, 252);"><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Check the coupon period and time </font></font></span><span lang="EN-US" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">,&nbsp; </font></font></span><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">simply use a free coupon issued by booking into their shop </font></font></span><span lang="EN-US" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.</font></font></span><span lang="EN-US" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none;"><o:p style="box-sizing: border-box;"></o:p></span></p><p class="a" style="box-sizing: border-box; border: 0px; text-size-adjust: none; color: rgb(51, 51, 51); font-family: &quot;Noto Sans KR&quot;, &quot;Apple SD Gothic Neo&quot;, &quot;Malgun Gothic&quot;, &quot;맑은 고딕&quot;, dotum, 돋움, sans-serif; font-size: 14px; background-color: rgb(252, 252, 252);"><span lang="EN-US" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- In&nbsp; </font></font></span><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">the case of a shop that pays&nbsp; </font></font></span><font style="vertical-align: inherit;"><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;">directly </font></span></font><span lang="EN-US" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">: Make&nbsp; </font></font></span><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">a reservation using a free coupon issued through a direct payment reservation</font></font></span><span lang="EN-US" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none;"><o:p style="box-sizing: border-box;"></o:p></span></p><p class="a" style="box-sizing: border-box; border: 0px; text-size-adjust: none; color: rgb(51, 51, 51); font-family: &quot;Noto Sans KR&quot;, &quot;Apple SD Gothic Neo&quot;, &quot;Malgun Gothic&quot;, &quot;맑은 고딕&quot;, dotum, 돋움, sans-serif; font-size: 14px; background-color: rgb(252, 252, 252);"><span lang="EN-US" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- In&nbsp; </font></font></span><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">the case of shops that do not proceed directly with payment&nbsp; </font></font></span><span lang="EN-US" style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">:&nbsp; </font></font></span><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">make a reservation using a free coupon issued through a phone reservation</font></font></span></p><p class="a" style="box-sizing: border-box; border: 0px; text-size-adjust: none; color: rgb(51, 51, 51); font-family: &quot;Noto Sans KR&quot;, &quot;Apple SD Gothic Neo&quot;, &quot;Malgun Gothic&quot;, &quot;맑은 고딕&quot;, dotum, 돋움, sans-serif; font-size: 14px; background-color: rgb(252, 252, 252);"><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none;">&nbsp;</span></p><p class="a" style="box-sizing: border-box; border: 0px; text-size-adjust: none; color: rgb(51, 51, 51); font-family: &quot;Noto Sans KR&quot;, &quot;Apple SD Gothic Neo&quot;, &quot;Malgun Gothic&quot;, &quot;맑은 고딕&quot;, dotum, 돋움, sans-serif; font-size: 14px; background-color: rgb(252, 252, 252);"><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none; font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">It can only be used at the shop where you left a review and cannot be replaced with another shop.&nbsp;</font></font></span></p><p class="a" style="box-sizing: border-box; border: 0px; text-size-adjust: none; color: rgb(51, 51, 51); font-family: &quot;Noto Sans KR&quot;, &quot;Apple SD Gothic Neo&quot;, &quot;Malgun Gothic&quot;, &quot;맑은 고딕&quot;, dotum, 돋움, sans-serif; font-size: 14px; background-color: rgb(252, 252, 252);"><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none;">&nbsp;</span></p><p class="a" style="box-sizing: border-box; border: 0px; text-size-adjust: none; color: rgb(51, 51, 51); font-family: &quot;Noto Sans KR&quot;, &quot;Apple SD Gothic Neo&quot;, &quot;Malgun Gothic&quot;, &quot;맑은 고딕&quot;, dotum, 돋움, sans-serif; font-size: 14px; background-color: rgb(252, 252, 252);"><span style="box-sizing: border-box; margin: 0px; padding: 0px; border: 0px; text-size-adjust: none;"><span style="color: rgb(0, 0, 0); font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">※ Please understand that the free coupon event may inevitably end depending on the circumstances of the shop </font></font></span><span lang="EN-US" style="color: rgb(0, 0, 0); font-family: 돋움, dotum; font-size: 9pt;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">.</font></font></span>&nbsp;</span></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">thank you.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Matong Manager -</font></font></p><p>&nbsp;</p><p>&nbsp;</p>
                </div>
            </div>
        </li>
        <li>
            <div class="info">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#askFixed19" role="button" aria-expanded="false" aria-controls="askFixed19">
                    <div class="tit-area">
                        <p class="tit"><span class="type"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[Member Information] </font></font></span> <span class="normal"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">How do I cancel my membership?</font></font></span></p>
                    </div>
                </a>
            </div>
            <div class="detail collapse" id="askFixed19" data-parent="#accordionAsk">
                <div class="box">
                    <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">At the bottom of the app &gt; More &gt; Edit my information &gt; 'Cancel membership' at the bottom of the app</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">When canceling membership, be sure to check the precautions before proceeding with the membership cancellation.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">※caution</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Account information cannot be recovered when you cancel your membership. </font><font style="vertical-align: inherit;">(Re-registration possible after 24 hours of membership withdrawal)</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- All coupons and points currently in possession are expired and cannot be recovered.</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- You will not be able to receive the benefits of your first subscription if you re-join after leaving. </font><font style="vertical-align: inherit;">(New member coupons, coupons for the first payment, etc.)</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Registered reviews are not automatically deleted. </font><font style="vertical-align: inherit;">Please delete them individually before withdrawing.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">thank you.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Matong Manager -</font></font></p>
                </div>
            </div>
        </li>
        <li>
            <div class="info">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#askFixed18" role="button" aria-expanded="false" aria-controls="askFixed18">
                    <div class="tit-area">
                        <p class="tit"><span class="type"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[Member Information] </font></font></span> <span class="normal"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">How do I edit member information?</font></font></span></p>
                    </div>
                </a>
            </div>
            <div class="detail collapse" id="askFixed18" data-parent="#accordionAsk">
                <div class="box">
                    <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">You can edit your information in More &gt; Edit my information at the bottom of the app.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Change of nickname: It is difficult to change a nickname that is already in use by another member.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Change password: You can set only 8 or more characters including English and numeric characters.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">thank you.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Matong Manager -</font></font></p>
                </div>
            </div>
        </li>
        <li>
            <div class="info">
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#askFixed17" role="button" aria-expanded="false" aria-controls="askFixed17">
                    <div class="tit-area">
                        <p class="tit"><span class="type"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">[Member Information] </font></font></span> <span class="normal"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">I can't remember ID and password</font></font></span></p>
                    </div>
                </a>
            </div>
            <div class="detail collapse" id="askFixed17" data-parent="#accordionAsk">
                <div class="box">
                    <p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">find ID</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1. A member who signed up after authentication with a mobile phone number&nbsp;</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1) Click More at the bottom of the app &gt; Login &gt; Find ID</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2) When registering as a member, enter the verified mobile phone number and verify</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3) ID verification</font></font></p><p>&nbsp;</p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">find password</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1. Members who signed up after verifying their mobile phone number</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1) Click More at the bottom of the app &gt; Login &gt; Forgot password</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2) When registering as a member, enter the verified mobile phone number and verify&nbsp;</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3) Send temporary password to the mobile phone number</font></font></p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4) Log in with the temporary password &gt; Set a new password</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">If you are having trouble finding your ID/password even with the above method, please contact the Matong Customer Center.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">thank you.</font></font></p><p>&nbsp;</p><p><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">- Matong Manager -</font></font></p>
                </div>
            </div>
        </li>
        
    </ul>
    <ul class="pagination">
        <li><a href="javascript:;" class="arrow"><i class="icon-chevron-double-left"></i></a></li><li><a href="javascript:;" class="arrow"><i class="icon-chevron-left"></i></a></li><li class="active"><a href="javascript:;"><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1</font></font></span></a></li><li data-page="2"><a href="javascript:;"><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2</font></font></span></a></li><li data-page="3"><a href="javascript:;"><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3</font></font></span></a></li><li data-page="4"><a href="javascript:;"><span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4</font></font></span></a></li><li><a href="javascript:;" class="arrow"><i class="icon-chevron-right"></i></a></li><li><a href="javascript:;" class="arrow"><i class="icon-chevron-double-right"></i></a></li>
    </ul>
</div>

        </section>
    </div>

</div>

<?php include('inc/footer.php'); ?>