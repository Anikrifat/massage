<?php include('inc/header.php'); ?>
<style>

</style>
<div class="content mt-5 pt-5 ">
    <div class="con-wrap con-subpage">
        <?php include('inc/left-menu.php'); ?>
        <!-- 본문 -->
        <section class="sub-page">
            <h3 class="sub-page-tit">
                <span class="subject">
                    <font style="vertical-align: inherit;">
                        <font style="vertical-align: inherit;">Favorite Shop List</font>
                    </font>
                </span>
            </h3>
            <!-- 샵 목록 -->
            <article class="shop-area">
                <div class="shop-normal-title">
                    <div class="tit">
                        <p>
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;">Jjimhan Shop </font>
                            </font><span class="shop-count">
                                <font style="vertical-align: inherit;">
                                    <font style="vertical-align: inherit;">1</font>
                                </font>
                            </span>
                            <font style="vertical-align: inherit;">
                                <font style="vertical-align: inherit;"> dogs</font>
                            </font>
                        </p>
                    </div>

                    <button class="shop-all-delete CHOICED_STATUS" onclick="choiceAllDelete()">
                        <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Delete all</font>
                        </font>
                    </button>

                </div>
                <!-- 20개 까지 -->
                <ul class="shop-list half" id="choicelist">
                    <li>
                        <?php include('inc/service-box.php'); ?>

                    </li>
                    <li>
                        <?php include('inc/service-box.php'); ?>

                    </li>
                    <li>
                        <?php include('inc/service-box.php'); ?>

                    </li>
                    <li>
                        <?php include('inc/service-box.php'); ?>

                    </li>
                    <li>
                        <?php include('inc/service-box.php'); ?>

                    </li>
                    <li>
                        <?php include('inc/service-box.php'); ?>

                    </li>

                </ul>
            </article>
        </section>
    </div>

</div>

<?php include('inc/footer.php'); ?>