<?php include('inc/header.php'); ?>
<style>

</style>
<div class="content mt-5 pt-5 ">
    <div class="con-wrap con-subpage">
        <?php include('inc/left-menu.php'); ?>
        <section class="sub-page">
            <h3 class="sub-page-tit">
                <span class="subject"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">event</font></font></span>
            </h3>
            <div class="event-area">
                <ul class="tab-list event-tab">
                    <li class="active" onclick="eventsrcform('ing')">
                        <a href="javascript:;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Current events</font></font></a>
                    </li>
                    <li onclick="eventsrcform('end')">
                        <a href="javascript:;"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">closed event</font></font></a>
                    </li>
                </ul>
                <!-- 탭 데이터 ajax -->
                <form method="post" id="event_src" name="event_src" enctype="multipart/form-data" onsubmit="return false;">
                    <input type="hidden" name="eventpage" id="eventpage" value="1">
                </form>

                <div class="tab-content" id="eventlist">
    <ul class="event-list">
        <li>
            <a href="/pages/_5_service/service_event_read.php?num=47">
                <img src="https://cdn.msgtong.com/new_event/_EVENT_BANNER_1080x440__EVENT_BANNER_20210809143120.jpg" alt="August Matong Day Event">
            </a>
        </li>
        <li>
            <a href="/pages/_5_service/service_event_read.php?num=36">
                <img src="https://cdn.msgtong.com/new_event/_EVENT_BANNER_1080x440__EVENT_BANNER_20210504113303.jpg" alt="August Review Event">
            </a>
        </li>
        <li>
            <a href="/pages/_5_service/service_event_read.php?num=45">
                <img src="https://cdn.msgtong.com/new_event/_EVENT_BANNER_1080x440__EVENT_BANNER_20210720172049.jpg" alt="Matong Aesthetic / Waxing Experience Group Recruitment Event">
            </a>
        </li>
        <li>
            <a href="/pages/_5_service/service_event_read.php?num=41">
                <img src="https://cdn.msgtong.com/new_event/_EVENT_BANNER_1080x440__EVENT_BANNER_20210622172442.jpg" alt="June - Waxing Event">
            </a>
        </li>
        <li>
            <a href="/pages/_5_service/service_event_read.php?num=40">
                <img src="https://cdn.msgtong.com/new_event/_EVENT_BANNER_1080x440__EVENT_BANNER_20210622155947.jpg" alt="Get a massage, get a beauty treatment, and an event">
            </a>
        </li>
        <li>
            <a href="/pages/_5_service/service_event_read.php?num=38">
                <img src="https://cdn.msgtong.com/new_event/_EVENT_BANNER_1080x440__EVENT_BANNER_20210604180552.jpg" alt="Cancellation consolation coupon event">
            </a>
        </li>
        <li>
            <a href="/pages/_5_service/service_event_read.php?num=37">
                <img src="https://cdn.msgtong.com/new_event/_EVENT_BANNER_1080x440__EVENT_BANNER_20210504132153.jpg" alt="First payment event in May">
            </a>
        </li>
        <li>
            <a href="/pages/_5_service/service_event_read.php?num=31">
                <img src="https://cdn.msgtong.com/new_event/_EVENT_BANNER_1080x440__EVENT_BANNER_20210405181447.jpg" alt="Notice to report">
            </a>
        </li>
        
    </ul>
</div>
            </div>
        </section>
        
    </div>

</div>

<?php include('inc/footer.php'); ?>